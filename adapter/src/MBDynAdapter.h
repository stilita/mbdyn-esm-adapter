/*
 * MBDynAdapter.h
 *
 *  Created on: Feb 19, 2020
 *      Author: claudio
 */

#ifndef SRC_MBDYNADAPTER_H_
#define SRC_MBDYNADAPTER_H_


#include "MBDynConnector.h"
#include "MovingAverage.h"

#include <mbcxx.h>
#include <precice/SolverInterface.hpp>
#include <rapidjson/document.h>

class MBDynAdapter
{
private:
	//const char* json;

	rapidjson::Document document;

	precice::SolverInterface *interface = nullptr;

	int dim = 0;
	int meshID = 0;
	int vertexSize = 0;
	int forceID = 0;
	int displID = 0;

	int *vertexIDs = nullptr;

	double precice_dt = 0.0;
	double t = 0.0;

public:
	MBDynConnector *conn;
	MovingAverage *ma_force = nullptr;
	MovingAverage *ma_disp = nullptr;

	double *adapterCoords = nullptr;
	double *adapterForces =nullptr;
	// double *adapterFilteredForces = nullptr;
	double *adapterDisplacements = nullptr;

	MBDynAdapter(const char* filename);

	~MBDynAdapter(void);

	int initializeConnector(void);

	int initializeAdapter(void);

	void runSimulation(void);

	void finalize(void);

	void printVector(int type);

};


#endif /* SRC_MBDYNADAPTER_H_ */
