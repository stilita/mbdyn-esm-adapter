#!/bin/bash
cd ${0%/*} || exit 1    		    		# Run from this directory
. $WM_PROJECT_DIR/bin/tools/RunFunctions    # Tutorial run functions

# Fluid participant

# Run this script in one terminal and the "runSolid" script in another terminal.
# These scripts present how the two participants would be started manually.
# Alternatively, you may execute the "Allrun" script in one terminal.

# Run this script with "-parallel" for parallel simulations

# The script "Allclean" cleans-up the result and log files.

# 1 for true, 0 for false
parallel=0
if [ "$1" = "-parallel" ]; then
    parallel=1
fi

echo "Preparing and running the Fluid participant..."

#echo "waiting..."

#sleep 12000

casename="Fluid"

rm -rfv ${casename}/0/
cp -r ${casename}/0.orig/ $casename/0/
blockMesh -case ${casename}
blockMesh -case Fluid
cd Fluid
transformPoints -translate "(-0.25 -0.2 0.0)"
cd ..
checkMesh -case ${casename} | tee log.checkMesh

# Initialize the internal field with a potential flow
potentialFoam -case ${casename} | tee log.potentialFoam


# Run
cd ${casename}
	solver=$(getApplication)
	procs=$(getNumberOfProcessors)
cd ..
if [ $parallel -eq 1 ]; then
    decomposePar -force -case ${casename}
    mpirun -np $procs renumberMesh -overwrite -parallel -case ${casename} | tee log.renumber
    mpirun -np $procs $solver -parallel -case ${casename} 2>&1 | tee log.pimpleFoam
    # reconstructPar -case ${casename}
else
    $solver -case ${casename}
fi

# Workaround for issue #26
./removeObsoleteFolders.sh

echo ""
echo "### NOTE ### Make sure to use the correct solver for your OpenFOAM version! (pimpleFoam for OpenFOAM v1806, OpenFOAM 6, or newer, vs pimpleDyMFoam for older) You may change this in your Fluid/system/controlDict file, if needed."
