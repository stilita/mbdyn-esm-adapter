/*
 * InterfaceForce.h
 *
 *  Created on: Feb 25, 2021
 *      Author: claudio
 */

#ifndef SRC_INTERFACEFORCE_H_
#define SRC_INTERFACEFORCE_H_

#include <rapidjson/document.h>
#include<vector>

class InterfaceForce{

private:
	// rapidjson::Document *doc;

	enum class Force_type {
		NONE,
		NODAL,
		PRESSURE
	};

	enum class Time_type{
		NONE,
		SIN,
		COS,
		RAMP
	};

	Force_type f_type = Force_type::NONE;
	Time_type t_type = Time_type::NONE;

	std::vector<int> loaded_nodes;
	std::vector<double> load_components;

	double period = 0.0;
	std::vector<double> ramp;

	double computeCoefficient(float t);

public:
	InterfaceForce(rapidjson::Document *document);
	bool configured = false;

	void getInterfaceForces(double* forces, int vertexSize, float t);


};


#endif /* SRC_INTERFACEFORCE_H_ */
