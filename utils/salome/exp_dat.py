import salome, salome.geom.geomtools

#import GEOM
#from salome.geom import geomBuilder
#geompy = geomBuilder.New(salome.myStudy)

import SMESH  #, SALOMEDS
from salome.smesh import smeshBuilder
smesh = smeshBuilder.New(salome.myStudy)
#from salome.StdMeshers import StdMeshersBuilder

from salome.gui import helper

#import numpy
#import time
#import random
#import ast
#import csv
#import os
#import math


def FindElementType(mesh_dimension, nb_nodes_in_element, boundary = False):
    
    if boundary == True:
        mesh_dimension -= 1
        
    line_type = 3
    triangle_type = 5
    quadrilateral_type = 9
    tetrahedral_type = 10
    hexahedral_type = 12
    wedge_type = 13
    pyramid_type = 14
    
    element_type = None
    
    if mesh_dimension == 1:
        if nb_nodes_in_element == 2:
            element_type = line_type
    elif mesh_dimension == 2:
        if nb_nodes_in_element == 3:
            element_type = triangle_type
        if nb_nodes_in_element == 4:
            element_type = quadrilateral_type
    elif mesh_dimension == 3:
        if nb_nodes_in_element == 4:
            element_type = tetrahedral_type
        if nb_nodes_in_element == 5:
            element_type = pyramid_type
        if nb_nodes_in_element == 6:
            element_type = wedge_type
        if nb_nodes_in_element == 8:
            element_type = hexahedral_type
    else:
        print("mesh dimension not recognized")
        element_type = -1
        
    return element_type



def ExportDATFile( mesh, file = None):
    """
	
	
Description:
	Exports a mesh into an .su2 file readable by the CFD solver SU2
	

Arguments:
	# mesh 
		Description:       The mesh to export. 
		Type:              Mesh 
		GUI selection:     yes 
		Selection by name: yes 
		Recursive:         - 
		Default value:     None  

	# file 
		Description:       The name without extension of the amsh file to write. If equals None, the name of the mesh in the study tree is taken. 
		Type:              String 
		GUI selection:     - 
		Selection by name: - 
		Recursive:         - 
		Default value:     None  

	# only 
		Description:       The list of names of groups to export, excluding the others. 
		Type:              List of Strings 
		GUI selection:     - 
		Selection by name: - 
		Recursive:         - 
		Default value:     [None]  

	# ignore 
		Description:       The list of names of groups to ignore. 
		Type:              List of Strings 
		GUI selection:     - 
		Selection by name: - 
		Recursive:         - 
		Default value:     [None]  

Returned Values:
	"dim" value:    - 
	"single" value: - 
	Type:           - 
	Number:         - 
	Name:           -  

Conditions of use:
	The mesh has to be computed and to contain groups describing the desired boundary conditions (inlet, outlet, wall, farfield, etc.).
	

    """

    myMesh_ref = helper.getSObjectSelected()[0].GetObject()
    mesh = smesh.Mesh(myMesh_ref)
    
    # Check the input shape existence
    
    if "error" in [mesh] or None in [mesh]:
        return
    else:
        print("Mesh found")
        
    
    # Get the mesh name
    mesh_name = mesh.GetName()
    print("Mesh name: "+mesh_name)
    
    # Renumber elements and nodes
    mesh.RenumberNodes()
    mesh.RenumberElements()
    
    # Get nodes number and IDs
    nb_nodes_in_mesh = mesh.NbNodes()
    node_ids_in_mesh = mesh.GetNodesId()
    
    # Get edges IDs
    #edge_ids_in_mesh = mesh.GetElementsByType(SMESH.EDGE)
    #nb_edges_in_mesh = mesh.NbEdges()
    
    # Get faces IDs
    face_ids_in_mesh = mesh.GetElementsByType(SMESH.FACE)
    
    nb_faces_in_mesh = mesh.NbFaces()
    nb_triangles_in_mesh = mesh.NbTriangles()
    nb_quadrangles_in_mesh = mesh.NbQuadrangles()
    
    #filter_tri = mesh.GetFilter(SMESH.FACE, SMESH.FT_ElemGeomType, SMESH.Geom_TRIANGLE)
    #filter_qua = mesh.GetFilter(SMESH.FACE, SMESH.FT_ElemGeomType, SMESH.Geom_QUADRANGLE)
    
    # Get volumes IDs
    nb_volumes_in_mesh = mesh.NbVolumes()
    
    #nb_tetrahedrons_in_mesh = mesh.NbTetras()
    #nb_tetrahedrons_in_mesh = mesh.NbTetras()
    #nb_pyramids_in_mesh = mesh.NbPyramids()
    #nb_pyramids_in_mesh = mesh.NbPyramids()
    #nb_prisms_in_mesh = mesh.NbPrisms()
    #nb_hexahedrons_in_mesh = mesh.NbHexas()
    
    #volume_ids_in_mesh = mesh.GetElementsByType(SMESH.VOLUME)
    
    # Get mesh dimension
    if nb_volumes_in_mesh != 0:
        mesh_dimension = 3
        nb_elements_in_domain = nb_volumes_in_mesh
    else:
        mesh_dimension = 2
        nb_elements_in_domain = nb_faces_in_mesh
        
 
################################
    # Open the dat file
    
    if file == None:
        file = mesh_name
        
    with open("%s.dat"%(file), "w") as dat_file:
        dat_file.write('# Program NUVOLA GRID\n')
        dat_file.write(" {0:d}\n".format(nb_nodes_in_mesh))
        
        for node_id in node_ids_in_mesh:
            node_coordinate = mesh.GetNodeXYZ(node_id)
            dat_file.write(' {0:.12f}    {1:.12f}    {2:.12f}\n'.format(*node_coordinate))
        
        print(nb_elements_in_domain)
        
        #ids_tri = mesh.GetIdsFromFilter(filter_tri)
        #ids_qua = mesh.GetIdsFromFilter(filter_qua)
        
        dat_file.write(' {0:d}\n'.format(nb_elements_in_domain))
        
        for element_id_in_domain in face_ids_in_mesh:
            nb_nodes_in_element = mesh.GetElemNodes(element_id_in_domain)
            for node in nb_nodes_in_element:
                dat_file.write('  {0:d}'.format(node))
            dat_file.write('\n')
            
