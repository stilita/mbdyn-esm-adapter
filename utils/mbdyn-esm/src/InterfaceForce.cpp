/*
 * InterfaceForce.cpp
 *
 *  Created on: Feb 25, 2021
 *      Author: claudio
 */

#include<iostream>
#include<algorithm>
#include<cmath>
#include "InterfaceForce.h"


InterfaceForce::InterfaceForce(rapidjson::Document *document){

	bool force_config = false;
	bool time_config = true;

	if(document->HasMember("force-params")){
		// read force parameters
		const rapidjson::Value& forceParams = (*document)["force-params"];

		if(forceParams.HasMember("type")){
			if(forceParams["type"] == "nodal"){
				std::cout << "[InterfaceForce] force TYPE NODAL. " << std::endl;
				f_type = Force_type::NODAL;

				// read loaded nodes:

				if(forceParams.HasMember("nodes")){
					const rapidjson::Value& param_nodes = forceParams["nodes"];
					for (rapidjson::Value::ConstValueIterator itr = param_nodes.Begin(); itr != param_nodes.End(); ++itr){
						loaded_nodes.push_back(itr->GetInt());
					}
					std::cout << "[InterfaceForce] load on " << param_nodes.Size() << " nodes." << std::endl;

					// read force components

					const rapidjson::Value& param_cmp = forceParams["components"];

					if(forceParams.HasMember("components")){
						for (rapidjson::Value::ConstValueIterator itr = param_cmp.Begin(); itr != param_cmp.End(); ++itr){
							load_components.push_back(itr->GetDouble());
						}
						std::cout << "[InterfaceForce] load components  x:  " << load_components[0] << " y: " << load_components[1] << " z: " << load_components[2] << std::endl;

						force_config = true;

					}else{
						std::cout << "[InterfaceForce] ERROR: force COMPONENTS not found. " << std::endl;
					}

				}else{
					std::cout << "[InterfaceForce] ERROR: force NODES not found. " << std::endl;
				}

			}else if(forceParams["type"] == "pressure"){
				f_type = Force_type::PRESSURE;
				std::cout << "[InterfaceForce] ERROR: force TYPE not yet implemented. " << std::endl;
			}else{
				std::cout << "[InterfaceForce] ERROR: force TYPE recognized. " << std::endl;
			}
		}else{
			std::cout << "[InterfaceForce] ERROR: force TYPE not found. " << std::endl;
		}
	}else{
		std::cout << "[InterfaceForce] ERROR: force configuration parameters not found. " << std::endl;
	}

	if(document->HasMember("time-params")){
		// read force parameters
		const rapidjson::Value& timeParams = (*document)["time-params"];

		if(timeParams.HasMember("type")){
			if(timeParams["type"] == "sin"){
				t_type = Time_type::SIN;
				std::cout << "[InterfaceForce] time TYPE SIN. " << std::endl;
			}else if(timeParams["type"] == "cos"){
				t_type = Time_type::COS;
				std::cout << "[InterfaceForce] time TYPE 0.5*(1-cos). " << std::endl;
			}else if(timeParams["type"] == "ramp"){
				t_type = Time_type::RAMP;
				std::cout << "[InterfaceForce] time TYPE limited ramp. " << std::endl;
			}else{
				std::cout << "[InterfaceForce] ERROR: time TYPE not found. " << std::endl;
				time_config = false;
			}
		}else{
			std::cout << "[InterfaceForce] ERROR: time TYPE not found. " << std::endl;
		}

		if(t_type == Time_type::SIN || t_type == Time_type::COS){
			if(timeParams.HasMember("period")){
				period = timeParams["period"].GetDouble();
				std::cout << "[InterfaceForce] load period: " << period << std::endl;
			}else{
				std::cout << "[InterfaceForce] ERROR: time SIN or COS PERIOD not found. " << std::endl;
				time_config = false;
			}
		}else{
			// we should only be in ramp here
			if(timeParams.HasMember("params")){
				// period = timeParams["period"].GetFloat();
				for (rapidjson::Value::ConstValueIterator itr = timeParams["params"].Begin(); itr != timeParams["params"].End(); ++itr){
					ramp.push_back(itr->GetDouble());
				}
				std::cout << "[InterfaceForce] RAMP PARAMETERS  slope:  " << ramp[0] << " initial time: " << ramp[1] << " final time: " << ramp[2] << " initial value: " << ramp[3] << std::endl;
				force_config = true;

			}else{
				std::cout << "[InterfaceForce] ERROR: time RAMP PARAMS not found. " << std::endl;
				time_config = false;
			}

		}
	}else{
		std::cout << "[InterfaceForce] ERROR: time configuration parameters not found. " << std::endl;
	}

	configured = (force_config and time_config);

	if(configured){
		std::cout << "[InterfaceForce] configured. " << std::endl;
	}else{
		std::cout << "[InterfaceForce] ERROR: NOT configured. " << std::endl;
	}

	srand( 0 );

};


double InterfaceForce::computeCoefficient(float t){

	double coeff = 0.0;

	switch (t_type) {
		case Time_type::COS:
			coeff = 0.5 * (1.0 - cos(2 * M_PI / period * t));
			break;
		case Time_type::SIN:
			coeff = sin(2 * M_PI / period * t);
			break;
		case Time_type::RAMP:

			// less than initial time
			if(t <= ramp[1]){
				// initial value
				coeff = ramp[3];
			// less than final time
			}else if(t <= ramp[2]){
				// initial value + ramp*delta t
				coeff = ramp[3] + ramp[0]*(t - ramp[1]);
			}else{
				// final value
				coeff = ramp[3] + ramp[0]*(ramp[2] - ramp[1]);
			}
			break;
		default:
			break;
	}

	std::cout << "[InterfaceForce] current time coefficient: " << coeff << std::endl;

	return coeff;
};


void InterfaceForce::getInterfaceForces(double *forces, int vertexSize, float t){

	double timeCoeff = computeCoefficient(t);

	timeCoeff += (((double) rand()/RAND_MAX)-0.5)*0.05;

	std::cout << "[Interface-force] current CORRECTED time coefficient: " << timeCoeff << std::endl;

	for (int currentVertex = 0; currentVertex < vertexSize; ++currentVertex) {
		if(std::find(loaded_nodes.begin(),loaded_nodes.end(),currentVertex) != loaded_nodes.end()){
			for (int currentComponent = 0; currentComponent < 3; ++currentComponent) {
				forces[3*currentVertex+currentComponent] = timeCoeff*load_components[currentComponent];
			}
		}else{
			for (int currentComponent = 0; currentComponent < 3; ++currentComponent) {
				forces[3*currentVertex+currentComponent] = 0.0;
			}
		}

	}

};



