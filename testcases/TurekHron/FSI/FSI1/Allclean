#!/bin/sh
cd ${0%/*} || exit 1    # Run from this directory

echo "Cleaning..."

# Source tutorial clean functions
. $WM_PROJECT_DIR/bin/tools/CleanFunctions

# Participant 1: Fluid
Participant1="Fluid"
cd ${Participant1}
    # Clean the case
    cleanCase
    rm -rfv 0
    # Create an empty .foam file for ParaView
    # Note: ".foam" triggers the native OpenFOAM reader of ParaView.
    # Change to ".OpenFOAM" to use the OpenFOAM reader provided with OpenFOAM.
    touch ${Participant1}.foam
cd ..
# Remove the log files
rm -fv log.checkMesh
rm -fv log.potentialFoam
rm -fv log.pimpleFoam
rm -rfv log.pimpleFoam.*
rm -fv log.renumber
# rm -fv ${Participant1}_reconstructPar.log

echo "Clean meshes..."

pfm="./PreCICE-Fluid-mesh"
if [ -d $pfm ]; then
    cd $pfm
    rm -rf *.vt*
    rm -rf *.pvtu
    cd ..
fi

psm="./PreCICE-Solid-mesh"
if [ -d $psm ]; then
    cd $psm
    rm -fv *.vtk
    cd ..
fi


# Participant 2: Solid
Participant2="MBDyn"
Solver2="Solid"
cd ${Participant2}
    # Clean the case
cd ./output
rm -fv *.vtu
rm -fv *.txt
cd ../mbd
rm -fv log.mbdyn
cd ..
	rm -fv *.act
        rm -fv *.frc
        rm -fv *.ine
        rm -fv *.jnt
        rm -fv *.log
        rm -rf *.mov
        rm -rf *.out

# Remove the log files
echo "Deleting Precice ${Solver2} files"
rm -fv precice-${Solver2}*

cd ..
# Remove the preCICE-related log files
echo "Deleting the preCICE log files..."


rm -fv \
    precice-${Participant1}*.log precice-${Solver2}*.log \
    precice-postProcessingInfo.log \
    precice-*-events.json

# Remove the preCICE address files
rm -rfv precice-run
rm -fv .*.address

echo "Cleaning complete!"
#------------------------------------------------------------------------------
