# body: BODY_LABEL, NODE_LABEL,
#    mass
#    reference node offset
#    inertia tensor



body: 1000+ROOT, ROOT,
	m_h,
	reference, node, dL/4, 0.0, 0.0,
	diag, Ix_h, Iy_h, Iz_h;
