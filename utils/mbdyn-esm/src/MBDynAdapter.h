/*
 * MBDynAdapter.h
 *
 *  Created on: Feb 19, 2020
 *      Author: claudio
 */

#ifndef SRC_MBDYNADAPTER_H_
#define SRC_MBDYNADAPTER_H_


#include "MBDynConnector.h"
#include "InterfaceForce.h"

#include <mbcxx.h>

#include <rapidjson/document.h>

class MBDynAdapter
{
private:

	rapidjson::Document document;

	int dim = 0;
	int meshID = 0;
	int vertexSize = 0;
	int forceID = 0;
	int displID = 0;

	// int *vertexIDs = nullptr;

	double dt = 0.0;
	double t = 0.0;


	int maxRepeat = 1;

	InterfaceForce *dummyCFD;


public:
	MBDynConnector *conn;

	double *adapterCoords = nullptr;
	double *adapterForces =nullptr;

	double *adapterDisplacements = nullptr;

	MBDynAdapter(const char* filename);

	~MBDynAdapter(void);

	int initializeConnector(void);

	int initializeAdapter(void);

	void runSimulation(void);

	void finalize(void);

	void printVector(int type);

	void writeTip(std::ofstream& fileName, std::vector<unsigned int> nodes, float time, std::string comment);

};


#endif /* SRC_MBDYNADAPTER_H_ */
