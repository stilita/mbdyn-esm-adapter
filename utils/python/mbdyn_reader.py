# -*- coding: utf-8 -*-
"""
Created on Wed Mar  4 13:36:45 2020

@author: claudio.caccia
"""

import numpy as np
import matplotlib.pyplot as plt
import matplotlib.cm as cm
from cycler import cycler
import os

colormap = cm.get_cmap("Spectral")


# function used to read act, ine mov data
def read_data(filename):
    if filename[-4:] == '.act':
        print('reading ACT file')
    elif filename[-4:] == '.ine':
        print('reading INE file')
    elif filename[-4:] == '.mov':
        print('reading MOV file')
    elif filename[-4:] == '.jnt':
        print('reading JNT file')
    else:
        print('not a recognized file type')
        return
    # data = np.genfromtxt(filename)
    data_dict = {}
    
    with open(filename, 'r') as df:
        line = df.readline().strip(' \n')
        while line:
            # print(line)
            row = line.split(' ')
            # print(row)
            if row[0] in data_dict:
                data_dict[row[0]] = np.append(data_dict[row[0]], np.array([[float(num) for num in row[1:]]]), axis=0)
            else:
                data_dict[row[0]] = np.array([[float(num) for num in row[1:]]])
            line = df.readline().strip(' \n')
    return data_dict


def read_frc(filename):
    if filename[-4:] != '.frc':
        filename += '.frc'
    
    # data = np.genfromtxt(filename)
    data_dict = {}
    
    with open(filename, 'r') as df:
        print('reading FRC file')
        line = df.readline().strip(' \n')
        while line:
            # print(line)
            row = line.split(' ')
            if '#' in row[0]:
                # print('Reference node')
                sep = '#'
                elem_node = row[0].split(sep)
                elem_node[0] = elem_node[0] + ' ref' 
                i_start = 1
            elif '@' in row[0]:
                # print('no reference node')
                sep = '@'
                elem_node = row[0].split(sep)
                i_start = 1
            else:
                elem_node = row[0:2]
                i_start = 2
            
            # print(elem_node)
            
            # print(row)
            if elem_node[0] in data_dict:
                # force element is present
                if elem_node[1] in data_dict[elem_node[0]]:
                    # force element is present and node is present
                    data_dict[elem_node[0]][elem_node[1]] = np.append(data_dict[elem_node[0]][elem_node[1]],
                                                                      np.array([[float(num) for num in row[i_start:]]]),
                                                                      axis=0)
                else:
                    
                    # force element is present but node not present
                    data_dict[elem_node[0]][elem_node[1]] = np.array([[float(num) for num in row[i_start:]]])
            else:
                # force element not present, should be the first and only entry
                # print('new force element')
                data_dict[elem_node[0]] = {}
                data_dict[elem_node[0]][elem_node[1]] = np.array([[float(num) for num in row[i_start:]]])
            line = df.readline().strip(' \n')
    return data_dict    


def plot_act(d_act, title='', filename=None, dt=0.001, select = np.ones((6,1),dtype=int), xlimits=None):
    
    lt_act = len(d_act[list(d_act)[0]][:, 0])

    num_nodes = len(d_act)
    colors = [plt.cm.Spectral(i) for i in np.linspace(0, 1, num_nodes)]

    label_act = ['axial force [N]', r'shear force [$T_y$]', r'shear force [$T_z$]',
                 r'moment [$M_x$]', r'moment [$M_y$]', r'moment [$M_z$]']
    
    t = np.arange(0, 50, dt)
    
    numplots = np.sum(select)
    
    plt.figure(figsize=(16, 10*numplots))
    
    curr_plot = 0
    
    for i, val in enumerate(select):
        
        if val > 0:
            curr_plot += 1
            ax = plt.subplot(numplots, 1, curr_plot)
            
            ax.set_prop_cycle(cycler('color', colors))
            
            for node in d_act:
                ax.plot(t[:lt_act], d_act[node][:, i], label='beam: '+node)
            #handles, labels = ax.get_legend_handles_labels()
            #lgd = ax.legend(handles, labels, bbox_to_anchor=(1.04,1), loc="upper left",ncol=2)
            ax.legend(loc="upper right",ncol=2)
            ax.set_xlabel('t [s]', fontsize=14)
            ax.set_ylabel(label_act[i], fontsize=14)
            if curr_plot == 1:
                ax.set_title(title, fontsize=16)
            ax.grid();
            if xlimits is not None:
                ax.set_xlim(xlimits)
            if i == 0:
                ax.set_ylim(0,2.5)
        
        if filename is not None:
            plt.savefig(filename+'_act.png')#,bbox_extra_artists=(lgd), bbox_inches='tight')


def plot_frc(d_frc, title='', filename=None, dt=0.001, force_set='1', ext_struct=True, select = np.ones((6,1),dtype=int)):
    lt_frc = len(d_frc[force_set][list(d_frc[force_set])[0]][:, ])

    num_nodes = len(d_frc[force_set])
    colors = [plt.cm.Spectral(i) for i in np.linspace(0, 1, num_nodes)]


    label_frc = [r'$F_x$', r'$F_y$', r'$F_z$', r'$M_x$', r'$M_y$', r'$M_z$']
    label_frc_nodal = [r'$F_x$', r'$F_y$', r'$F_z$', r'$x$ position', r'$y$ position', r'$z$ position']
    
    t = np.arange(0, 50, dt)
    
    numplots = np.sum(select)
    
    plt.figure(figsize=(16, 10*numplots))
    
    curr_plot = 0
    
    for i, val in enumerate(select):
        
        if val > 0:
            curr_plot += 1
            
            ax  = plt.subplot(6, 1, i+1)

            ax.set_prop_cycle(cycler('color', colors))
            
            for node in d_frc[force_set]:
                ax.plot(t[:lt_frc], d_frc[force_set][node][:, i], label='node: '+node)
            handles, labels = ax.get_legend_handles_labels()
            lgd = ax.legend(handles, labels, bbox_to_anchor=(1.04,1), loc="upper left",ncol=2)
            ax.set_xlabel('t [s]', fontsize=14)
            if ext_struct:
                ax.set_ylabel(label_frc[i], fontsize=14)
            else:
                ax.set_ylabel(label_frc_nodal[i], fontsize=14)
            if curr_plot == 1:
                ax.set_title(title, fontsize=16)
            ax.grid();
        if filename is not None:
            plt.savefig(filename+'_frc.png',bbox_extra_artists=(lgd,), bbox_inches='tight')


def plot_ine(d_ine, title='', filename=None, dt=0.001, select=(True, True)):
    lt_ine = len(d_ine[list(d_ine)[0]][:, 0])
    
    num_nodes = len(d_ine)
    colors = [plt.cm.Spectral(i) for i in np.linspace(0, 1, num_nodes)]
    
    label_ine = [r'$m_x$', r'$m_y$', r'$m_z$', r'$L_x$', r'$L_y$', r'$L_z$',
                 r'$\dot{m}_x$', r'$\dot{m}_y$', r'$\dot{m}_z$', r'$\dot{L}_x$',
                 r'$\dot{L}_y$', r'$\dot{L}_z$']
    
    t = np.arange(0, 50, dt)
    
    for kk, val in enumerate(select): 
        if val:
            plt.figure(figsize=(16, 72))
            for i in range(6):
                ax = plt.subplot(6, 1, i+1)
		
                ax.set_prop_cycle(cycler('color', colors))
		
                for node in d_ine:
                    ax.plot(t[:lt_ine], d_ine[node][:, 6*kk+i], label='node: '+node)
                ax.legend(bbox_to_anchor=(1.04,1), loc="upper left",ncol=2)
                ax.set_xlabel('t [s]', fontsize=14)
                ax.set_ylabel(label_ine[6*kk+i], fontsize=14)
                if i == 0:
                    ax.set_title(title, fontsize=16)
                ax.grid();
                if filename is not None:
                    if kk == 0:
                        plt.savefig(filename+'_ine.png')
                    else:
                        plt.savefig(filename+'_dot_ine.png')


def plot_mov(d_mov, title='', filename=None, dt=0.001, select=(True, True)):
    lt_mov = len(d_mov[list(d_mov)[0]][:, 0])
    
    num_nodes = len(d_mov)
    colors = [plt.cm.Spectral(i) for i in np.linspace(0, 1, num_nodes)]
    
    label_mov = [r'$x$', r'$y$', r'$z$', r'$\theta_1$', r'$\theta_2$', r'$\theta_3$',
                 r'$\dot{x}$', r'$\dot{y}$', r'$\dot{z}$', r'$\dot{\theta}_1$',
                 r'$\dot{\theta}_2$', r'$\dot{\theta}_3$']
    
    t = np.arange(0, 50, dt)
    
    for kk, val in enumerate(select): 
        if val:
            plt.figure(figsize=(16, 72))
            for i in range(6):
                ax = plt.subplot(6, 1, i+1)
		
                ax.set_prop_cycle(cycler('color', colors))
		
                for node in d_mov:
                    ax.plot(t[:lt_mov], d_mov[node][:, 6*kk+i], label='node: '+node)
                ax.legend(bbox_to_anchor=(1.04,1), loc="upper left",ncol=2)
                ax.set_xlabel('t [s]', fontsize=14)
                ax.set_ylabel(label_mov[6*kk+i], fontsize=14)
                if i == 0:
                    ax.set_title(title, fontsize=16)
                ax.grid();
                if filename is not None:
                    if kk == 0:
                        plt.savefig(filename+'_mov.png')
                    else:
                        plt.savefig(filename+'_dot_mov.png')


def plot_jnt(d_jnt, title='', filename=None, dt=0.001, select=(True, True, True, True)):
    lt_jnt = len(d_jnt[list(d_jnt)[0]][:, 0])
    
    num_nodes = len(d_jnt)
    colors = [plt.cm.Spectral(i) for i in np.linspace(0, 1, num_nodes)]
    
    label_jnt = [r'$R_X$', r'$R_Y$', r'$R_Z$', r'$Mr_X$', r'$Mr_Y$', r'$Mr_Z$',
                 r'$R_X$ global', r'$R_Y$ global', r'$R_Z$ global', r'$Mr_X$ global', r'$Mr_Y$ global', r'$Mr_Z$ global',
                 r'$\Delta X$', r'$\Delta Y$', r'$\Delta Z$', r'$\Delta\theta_1$', r'$\Delta\theta_2$', r'$\Delta\theta_3$',
                 r'$\Delta X$ global', r'$\Delta Y$ global', r'$\Delta Z$ global',
                 r'$\Delta\theta_1$ global', r'$\Delta\theta_2$ global', r'$\Delta\theta_3$ global']
    
    t = np.arange(0, 50, dt)
     
    for kk, val in enumerate(select):
        if val:
            plt.figure(figsize=(16, 54))
            for i in range(6):
                ax = plt.subplot(6, 1, i+1)
		
                ax.set_prop_cycle(cycler('color', colors))
		
                for node in d_jnt:
                    if np.shape(d_jnt[node])[1] > 6*kk+i:
                        # print('joint: {} - index {}'.format(node,i))
                        plt.plot(t[:lt_jnt], d_jnt[node][:, 6*kk+i], label='joint: '+node)
                ax.legend(bbox_to_anchor=(1.04,1), loc="upper left",ncol=2)
                ax.set_xlabel('t [s]', fontsize=14)
                ax.set_ylabel(label_jnt[6*kk+i], fontsize=14)
                if i == 0:
                    ax.set_title(title, fontsize=16)
                ax.grid();
                if filename is not None:
                    if kk == 0:
                        plt.savefig(filename+'_jnt.png')
                    elif kk == 1:
                        plt.savefig(filename+'_jnt_glob.png')
                    elif kk == 2:
                        plt.savefig(filename+'_TJ_jnt.png')
                    else:
                        plt.savefig(filename+'_TJ_jnt_glob.png')


def plot_shape(d_mov, title='', filename=None):
    pass


if __name__ == '__main__':
    os.chdir('./d0001_da001_j11_F0')
    
    read_frc('cnt_out')
    
    os.chdir('../')
