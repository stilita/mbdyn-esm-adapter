import argparse

import numpy as np
import precice
import vtk

parser = argparse.ArgumentParser()
parser.add_argument("configurationFileName", help="Name of the xml config file.", type=str)
parser.add_argument("meshName", help="Name of the mesh.", type=str)


try:
    args = parser.parse_args()
except SystemExit:
    print("")
    print("Usage: python3 ./struct_stub.py precice-config mesh-name")
    quit()


class StubBeamAdapter:
    def __init__(self, config_file_name, mesh_file_name):

        self.interface = precice.Interface("Solid", 0, 1)  # proc no, procs

        #print("Interface set")
        self.interface.configure(config_file_name)
        print("Interface configured")
        self.dim = self.interface.get_dimensions()
        #print("Get dimensions: {}".format(self.dim))

        filename = mesh_file_name
        reader = vtk.vtkGenericDataObjectReader()
        reader.SetFileName(filename)
        reader.Update()

        self.nodes = np.array(reader.GetOutput().GetPoints().GetData())
        self.num_nodes = np.size(self.nodes, 0)

        print("Struct num nodes : {}".format(self.num_nodes))

        #        with open(mesh_file_name, 'r') as mf:
        #
        #            print("Read mesh")
        #            r = mf.readline()
        #
        #            while r[0] == '#':
        #                r = mf.readline()
        #
        #            try:
        #                self.num_nodes = int(r)
        #            except ValueError as ve:
        #                print(ve)
        #                print("Number of nodes not found!")
        #                print("Extiting...")
        #                return
        #
        #           self.nodes = np.zeros((self.num_nodes, self.dim))
        #
        #            for i in range(self.num_nodes):
        #                point = mf.readline().split()
        #                if self.dim < len(point):
        #                    self.nodes[i, :] = [float(coord) for coord in point[0:self.dim]]
        #                else:
        #                    self.nodes[i, :] = [float(coord) for coord in point]

        #print("Mesh read")

        self.mesh_id = self.interface.get_mesh_id("Solid-Mesh")

        #print("get mesh id: {}".format(self.mesh_id))


        #self.nodeVertexIDs = np.zeros((self.num_nodes,1))
        #self.nodeVertexIDs = np.arange(self.num_nodes)
        self.nodeVertexIDs = self.num_nodes*[0.0]
        self.interface.set_mesh_vertices(self.mesh_id, self.num_nodes, np.ravel(self.nodes[:, :self.dim]),
                                         self.nodeVertexIDs)
        
        #print("set vertices at interface")
        
        self.initial_coords = self.nodes.copy()
        
        self.displacements = np.array(self.dim * self.num_nodes * [0.0])
        self.force = np.zeros_like(self.displacements) #np.array(self.dim * self.num_nodes * [0.0])
        #self.prev_displacements = np.zeros_like(self.nodes)

        #print("Initialized to 0.0 displacements and forces vectors")

        self.displacementsID = self.interface.get_data_id("Displacements0", self.mesh_id)
        self.forceID = self.interface.get_data_id("Forces0", self.mesh_id)

        #print("Get displacement and force IDs: {} {}".format(self.displacementsID, self.write_id))

        # print(self.nodes)

        # movement parameters:
        self.A0 = 0.02
        self.period = 2.0
        self.x0 = np.min(self.nodes[:, 0])
        self.xf = np.max(self.nodes[:, 0])
        self.y0 = np.min(self.nodes[:, 1])
        self.yf = np.max(self.nodes[:, 1])
        self.L = self.xf - self.x0
        self.h = self.yf - self.y0
        self.ym = (self.yf + self.y0)/2.0
        
        self.dt = 0.001
        self.start_time = 0.02

        print("Read x0, xf, compute L: {} {} {}".format(self.x0, self.xf, self.L))

        #print("before initializing")

        self.precice_dt = self.interface.initialize()

        print("Interface initialized with dt: {}".format(self.dt))

        self.time = 0.0

        #print("ask if data is to be written")

        if self.interface.is_action_required(precice.action_write_initial_data()):
            self.interface.write_block_vector_data(self.displacementsID, self.num_nodes, self.nodeVertexIDs,
                                                   self.displacements)
            self.interface.fulfilled_action(precice.action_write_initial_data())

        #print("initialize data")
        self.interface.initialize_data()

        #print("ask if data is to be read")
        if self.interface.is_read_data_available():
            self.interface.read_block_vector_data(self.forceID, self.num_nodes, self.nodeVertexIDs, self.force)

        #print("End setup")

    def run_precice(self):
        print("Start running...")
        iteration = 0
        # previous_displacements = self.nodes
        #print("Initialize current displacements")
        act_displacements = np.zeros_like(self.nodes)

        #print("Before coupling going on... (STEP 1)")

        while self.interface.is_coupling_ongoing():
            if self.interface.is_action_required(precice.action_write_iteration_checkpoint()):
                self.interface.fulfilled_action(precice.action_write_iteration_checkpoint())
            if self.dim == 2:
                # print("PIPPO")
                f = np.zeros((self.num_nodes, 3))
                f[:, :self.dim] = np.reshape(self.force, (-1, self.dim))
            else:
                f = np.reshape(self.force, (-1, 3))

            #print("Forces set ...(STEP 2)")

            # self.mbd.setForces(f)
            # if self.mbd.solve(False):
            #    break

            # displacements = self.mbd.getDisplacements()
            if self.time < self.start_time:
                print("do nothing at beginning")
                #displacements = np.zeros_like(self.nodes)
            else:
                #print("compute deformation")
                At = self.A0 * (1.0 - np.cos(2 * np.pi / self.period * (self.time - self.start_time)))
                angle = np.arctan(2*At/(self.L**2)*(self.initial_coords[:, 0] - self.x0))
                act_displacements[:, 0] = np.zeros_like(self.nodes[:, 1]) -(self.initial_coords[:, 1] -self.ym )*np.sin(angle)
                act_displacements[:, 1] = At * ( (self.initial_coords[:, 0] - self.x0) / self.L) ** 2 # + (self.initial_coords[:, 1] - self.ym) *np.cos(angle)
            
            print("tip displacement: {}".format(act_displacements[-1,1]))
            
            #rel_displacements = act_displacements - self.prev_displacements
            # print(displacements)
            # print(np.ravel(displacements))

            #print("Write displacements...(STEP 3)")

            self.interface.write_block_vector_data(self.displacementsID, self.num_nodes, self.nodeVertexIDs,
                                                   np.ravel(act_displacements))

            #print("Advance in time...(STEP 4)")

            self.act_dt = min(self.dt, self.precice_dt)

            self.precice_dt = self.interface.advance(self.act_dt)

            #print("Read forces...(STEP 5)")

            self.interface.read_block_vector_data(self.forceID, self.num_nodes, self.nodeVertexIDs, self.force)

            #print("Check advance...(STEP 6)")

            if self.interface.is_action_required(precice.action_read_iteration_checkpoint()):  # i.e. not yet converged
                self.interface.fulfilled_action(precice.action_read_iteration_checkpoint())
                #print("not yet converged")
                pass
            else:
                iteration += 1
                #self.prev_displacements = act_displacements
                self.time += self.precice_dt
                #print("converged")
                # break
            # if self.mbd.solve(True):
            #    break
            # if iteration % self.mbd.controlDict['output frequency'] == 0:
            #    self.mbd.writeVTK(iteration)

        self.interface.finalize()


sba = StubBeamAdapter(args.configurationFileName, args.meshName)
sba.run_precice()
