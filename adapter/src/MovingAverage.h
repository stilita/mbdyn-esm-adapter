/*
 * MovingAverage.h
 *
 *  Created on: Mar 16, 2020
 *      Author: claudio
 */

#ifndef SRC_MOVINGAVERAGE_H_
#define SRC_MOVINGAVERAGE_H_


class MovingAverage {
public:
	double *filteredData;
	int windowSize;
	int rowsDim;

	MovingAverage(int window, int points);
	void getFilteredData(const double *currentData);
	~MovingAverage();
	void advance(void);

private:
	int current = 0;
	double coeff;
	double **data;
};



#endif /* SRC_MOVINGAVERAGE_H_ */
