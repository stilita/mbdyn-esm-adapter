import getopt
import json
import sys

import numpy as np
# from mpi4py import MPI
import precice
import vtk


def main(argv):
    try:
        opts, args = getopt.getopt(argv, "hf:", ["file="])
    except getopt.GetoptError:
        print('stub-adapter.py -f <inputfile.json>')
        sys.exit(2)
    for opt, arg in opts:
        if opt == '-h':
            print('stub-adapter.py -f <inputfile.json>')
            sys.exit()
        elif opt in ("-f", "--file"):
            inputfile = arg

    np.set_printoptions(precision=7)

    with open(inputfile, 'r') as f:
        config = json.load(f)

    fluid = StubAdapter(config)
    fluid.run()


class StubAdapter:
    def __init__(self, config):
        # interface = precice.Interface(solverName, configFileName, processRank, processSize)
        # last numbers: proc no, nprocs
        self.interface = precice.Interface(config["participant-name"], config["precice-config"], 0, 1)
        self.dim = self.interface.get_dimensions()
        self.num_nodes = 0
        self.read_mesh(config["mesh"])

        # after reading the mesh we can initialize the vtk grid
        self.grid = vtk.vtkUnstructuredGrid()
        self.initialize_vtk()

        # read or write deltas?
        self.is_delta = config["displacement-delta"]
        self.wrt_interval = config["write-interval"]
        self.vtk_output = config["vtk-output"]

        # true if the adapter writes forces and reads displacements
        self.force_adapter = config["force-adapter"]

        mesh_id = self.interface.get_mesh_id(config["mesh-name"])

        self.nodeVertexIDs = self.interface.set_mesh_vertices(mesh_id, self.nodes)

        self.force = np.zeros((self.num_nodes, self.dim))
        # total displacement
        self.displacement = np.zeros_like(self.force)
        # is_delta displacement from 2 iterations
        self.displacement_delta = np.zeros_like(self.force)
        # data from interface
        self.read_data = np.zeros_like(self.force)
        # data to interface
        self.write_data = np.zeros_like(self.force)

        self.read_id = self.interface.get_data_id(config["read-data-name"], mesh_id)
        self.write_id = self.interface.get_data_id(config["write-data-name"], mesh_id)

        if self.force_adapter:
            if config["force-params"]["type"] == "pressure":
                self.force_type = "pressure"
                self.press_value = config["force-params"]["value"]
                self.min_normal = config["force-params"]["min-normal"]
                self.max_normal = config["force-params"]["max-normal"]
            else:
                self.force_type = "nodal"
                self.force_nodes = config["force-params"]["nodes"]
                self.force_components = np.array(config["force-params"]["components"])
        else:
            # displacement adapter
            self.A0 = config["displacement"]["tip"]
            self.L = config["displacement"]["length"]
            plane = config["displacement"]["plane"]
            if plane[0] == "x":
                self.beam_axis = 0
                if plane[1] == "y":
                    self.beam_bend = 1
                elif plane[1] == "z":
                    self.beam_bend = 2
                else:
                    print("displacement plane error: " + plane + " not recognized.")
                    sys.exit(1)
            elif plane[0] == "y":
                self.beam_axis = 1
                if plane[1] == "x":
                    self.beam_bend = 0
                elif plane[1] == "z":
                    self.beam_bend = 2
                else:
                    print("displacement plane error" + plane + " not recognized.")
                    sys.exit(1)
            elif plane[0] == "z":
                self.beam_axis = 2
                if plane[1] == "x":
                    self.beam_bend = 0
                elif plane[1] == "y":
                    self.beam_bend = 1
                else:
                    print("displacement plane error" + plane + " not recognized.")
                    sys.exit(1)
            else:
                print("displacement plane error" + plane + " not recognized.")
                sys.exit(1)

        # read time parameters (i.e. how forces or displacements evolve in time)
        self.time_evol = config["time-params"]["type"]

        if self.time_evol == "cos":
            self.period = config["time-params"]["period"]
        elif self.time_evol == "sin":
            self.period = config["time-params"]["period"]
        elif self.time_evol == "ramp":
            self.slope = config["time-params"]["params"][0]
            self.initial_time = config["time-params"]["params"][1]
            self.final_time = config["time-params"]["params"][2]
            self.initial_value = config["time-params"]["params"][3]

        # root coordinates to compute moments
        self.root = np.array(config["root-coords"])

        self.t = 0.0
        self.iteration = 0
        self.dt = self.interface.initialize()

        if self.interface.is_action_required(precice.action_write_initial_data()):
            self.interface.write_block_vector_data(self.write_id, self.nodeVertexIDs, self.write_data)
            self.interface.mark_action_fulfilled(precice.action_write_initial_data())

        self.interface.initialize_data()

        if self.interface.is_read_data_available():
            self.read_data = self.interface.read_block_vector_data(self.read_id, self.nodeVertexIDs)

            # TODO: update for force and disp
            self.displacement = self.read_data.copy()
            self.displacement_delta = self.read_data.copy()
            self.coords = self.displacement + self.nodes.copy()
            print("data available")
        else:
            print("NO data available")
            self.coords = self.nodes.copy()

        if self.dim == 3:
            self.compute_areas_and_normals()

    def read_mesh(self, filename):
        with open(filename, 'r') as mf:

            print("Read mesh")
            r = mf.readline()

            while r[0] == '#':
                r = mf.readline()

            try:
                self.num_nodes = int(r)
            except ValueError as ve:
                print(ve)
                print("Number of nodes not found!")
                print("Extiting...")
                return

            self.nodes = np.zeros((self.num_nodes, self.dim))

            for i in range(self.num_nodes):
                point = mf.readline().split()
                if self.dim < len(point):
                    self.nodes[i, :] = [float(coord) for coord in point[0:self.dim]]
                else:
                    self.nodes[i, :] = [float(coord) for coord in point]

            # read number of cells
            r = mf.readline()
            try:
                self.num_cells = int(r)
            except ValueError as ve:
                print(ve)
                print("Number of cells not found!")
                print("Extiting...")
                return

            # minus one for the right hand rule
            self.cells = -1 * np.ones((self.num_cells, 4), dtype=np.int16)

            if self.dim == 3:
                self.cell_areas = np.zeros(self.num_cells)
                self.normals = np.zeros((self.num_cells, 3))

            for i in range(self.num_cells):
                connectivity = mf.readline().split()
                self.cells[i, 0:len(connectivity)] = [int(node) - 1 for node in connectivity]

        print("Mesh read")
        # print(self.nodes[0,:])
        # print(self.nodes[1, :])
        # print(self.nodes[14, :])
        # print(self.nodes[23, :])
        # print(self.nodes[12,:])
        # print(self.nodes[4, :])
        # print(self.nodes[5, :])
        # print(self.nodes[13, :])

    def compute_areas_and_normals(self):

        for i in range(self.num_cells):
            point_a = self.coords[self.cells[i, 0], :]
            point_b = self.coords[self.cells[i, 1], :]
            point_c = self.coords[self.cells[i, 2], :]

            segm_ab = point_b - point_a
            segm_ac = point_c - point_a
            segm_cb = point_c - point_b

            # first compute the unit vector aligned with ab
            n_ab = segm_ab / np.linalg.norm(segm_ab)

            # the distance vector is vector AC minus its projection along n_ab
            dist = segm_ac - np.dot(segm_ac, n_ab) * n_ab

            self.cell_areas[i] = np.linalg.norm(dist) * np.linalg.norm(segm_ab)

            if self.cells[i, 3] == -1:
                # it is a triangle
                self.cell_areas[i] = self.cell_areas[i] / 2.0

            tempv = np.cross(segm_ab, segm_cb)
            self.normals[i, :] = tempv / np.linalg.norm(tempv)

            # if i==44 or i==45:
            #     print("----AREA----")
            #     print("Point A: x:{0} y:{1} z{2}".format(point_a[0],point_a[1],point_a[2]))
            #     print("Point B: x:{0} y:{1} z{2}".format(point_b[0],point_b[1],point_b[2]))
            #     print("Point C: x:{0} y:{1} z{2}".format(point_c[0],point_c[1],point_c[2]))
            #     print("Segmento AB: {0} - lunghezza: {1}".format(segm_ab,np.linalg.norm(segm_ab)))
            #     print("Segmento BC: {0} - lunghezza: {1}".format(segm_bc,np.linalg.norm(segm_bc)))
            #     print("Normale: x:{0} y:{1} z:{2}".format(self.normals[i,0],self.normals[i,1],self.normals[i,2]))
            #     print("AREA: {0}".format(self.cell_areas[i]))

        print("normals computed. NB outward normal is supposed")
        # print(self.cell_areas[44:46])
        # print(self.normals)
        # print(self.nodes)
        # print(self.cells)
        # print(self.coords)
        # print(pippo)

    def set_forces_2d(self):
        self.force.fill(0)

        self.force[1, 1] = 0.0005
        self.force[2, 1] = 0.0005

    def set_forces(self):

        self.force.fill(0)

        if self.force_type == "nodal":
            f_nodal = self.compute_time_coefficient() * self.force_components
            self.force[self.force_nodes, :] = f_nodal
        elif self.force_type == "pressure":
            pressure = self.compute_time_coefficient() * self.press_value
            for i in range(self.num_cells):
                # consider only upper cells, with negative y normal component
                if (self.normals[i, :] >= self.min_normal).all() and (self.normals[i, :] <= self.max_normal).all():
                    if self.cells[i, 3] != -1:
                        ftemp = pressure * self.normals[i, :] * self.cell_areas[i] / 4.0
                        for k in range(4):
                            self.force[self.cells[i, k], :] += ftemp
                    else:
                        ftemp = pressure * self.normals[i, :] * self.cell_areas[i] / 3.0
                        for k in range(3):
                            self.force[self.cells[i, k], :] += ftemp

    def compute_resultants(self):
        root = np.zeros((3, 1))
        self.F = np.sum(self.force, axis=0)
        self.M = np.zeros_like(self.F)

        self.M[0] = np.dot(self.coords[:, 1] - self.root[1], self.force[:, 2]) - np.dot(
            self.coords[:, 2] - self.root[2], self.force[:, 1])
        self.M[1] = np.dot(self.coords[:, 2] - self.root[2], self.force[:, 0]) - np.dot(
            self.coords[:, 0] - self.root[0], self.force[:, 2])
        self.M[2] = np.dot(self.coords[:, 0] - self.root[0], self.force[:, 1]) - np.dot(
            self.coords[:, 1] - self.root[1], self.force[:, 0])

    def compute_time_coefficient(self):
        if self.time_evol == "cos":
            coeff = 0.5 * (1.0 - np.cos(2 * np.pi / self.period * self.t))
        elif self.time_evol == "sin":
            coeff = np.sin(2 * np.pi / self.period * self.t)
        elif self.time_evol == "ramp":
            if self.t <= self.initial_time:
                coeff = self.initial_value
            elif self.t <= self.final_time:
                coeff = self.initial_value + self.slope * (self.t - self.initial_time)
            else:
                coeff = self.initial_value + self.slope * (self.final_time - self.initial_time)
        else:
            coeff = 1.0
        return coeff

    def set_displacements(self):

        self.displacement = np.zeros_like(self.nodes)

        At = self.A0 * self.compute_time_coefficient()

        print("t = {0} - At = {1}".format(self.t, At))
        angle = np.arctan(2 * At / (self.L ** 2)) * self.nodes[:, self.beam_axis]
        self.displacement[:, self.beam_axis] = -self.nodes[:, self.beam_bend] * np.sin(angle)
        self.displacement[:, self.beam_bend] = At * (self.nodes[:, self.beam_axis] / self.L) ** 2 + \
                                               self.nodes[:, self.beam_bend] * (1 - np.cos(angle))

        self.displacement_delta = self.displacement - self.coords

    def run(self):

        self.write_vtk()

        while self.interface.is_coupling_ongoing():
            if self.interface.is_action_required(precice.action_write_iteration_checkpoint()):
                self.interface.mark_action_fulfilled(precice.action_write_iteration_checkpoint())

            if self.force_adapter:
                if self.dim == 3:
                    # self.set_nodal_force()
                    self.set_forces()
                else:
                    self.set_forces_2d()
                self.write_data = self.force.copy()
            else:
                self.set_displacements()
                if self.is_delta:
                    self.write_data = self.displacement_delta.copy()
                else:
                    self.write_data = self.displacement.copy()

            self.interface.write_block_vector_data(self.write_id, self.nodeVertexIDs, self.write_data)

            self.dt = self.interface.advance(self.dt)

            self.read_data = self.interface.read_block_vector_data(self.read_id, self.nodeVertexIDs)

            # if self.iteration > 11:
            #     print("FORCING STOP...")
            #     break
            if self.dim == 3:
                self.compute_areas_and_normals()

            if self.interface.is_action_required(precice.action_read_iteration_checkpoint()):  # i.e. not yet converged
                self.interface.mark_action_fulfilled(precice.action_read_iteration_checkpoint())
            else:
                if self.force_adapter:
                    if self.is_delta:
                        self.displacement_delta = self.read_data.copy()
                        self.displacement += self.displacement_delta
                    else:
                        self.displacement_delta = self.read_data - self.displacement
                        self.displacement = self.read_data.copy()

                    self.coords = self.nodes + self.displacement
                else:
                    self.force = self.read_data.copy()

                # if self.iteration % 10 == 0:
                self.iteration += 1
                if self.iteration % self.wrt_interval == 0:
                    self.write_vtk()
                self.t += self.dt

                print("iteration {}".format(self.iteration))

    def initialize_vtk(self):
        self.grid.Allocate(1, 1)

        points = vtk.vtkPoints()
        points.SetNumberOfPoints(self.num_nodes)

        for i in range(self.num_nodes):
            if self.dim == 3:
                points.InsertPoint(i, self.nodes[i, :])
            else:
                points.InsertPoint(i, np.append(self.nodes[i, :], 0.0))

        self.grid.SetPoints(points)

        for i in range(self.num_cells):
            if self.cells[i, 3] == -1:
                tri = vtk.vtkTriangle()
                for k in range(3):
                    tri.GetPointIds().SetId(k, self.cells[i, k])

                self.grid.InsertNextCell(tri.GetCellType(), tri.GetPointIds())
            else:
                quad = vtk.vtkQuad()
                for k in range(4):
                    quad.GetPointIds().SetId(k, self.cells[i, k])

                self.grid.InsertNextCell(quad.GetCellType(), quad.GetPointIds())

    def write_vtk(self):

        vtk_force = vtk.vtkDoubleArray()
        vtk_force.SetNumberOfComponents(3)
        # vtk_force.SetNumberOfValues(self.num_nodes)
        vtk_force.SetName("force")

        vtk_displacement = vtk.vtkDoubleArray()
        vtk_displacement.SetNumberOfComponents(3)
        # vtk_displacement.SetNumberOfValues(self.num_nodes)
        vtk_displacement.SetName("displacement")

        vtk_disp_delta = vtk.vtkDoubleArray()
        vtk_disp_delta.SetNumberOfComponents(3)
        # vtk_displacement.SetNumberOfValues(self.num_nodes)
        vtk_disp_delta.SetName("displacement-delta")

        if self.dim == 3:
            for i in range(self.num_nodes):
                vtk_force.InsertNextTuple(self.force[i, :])
                vtk_displacement.InsertNextTuple(self.displacement[i, :])
                vtk_disp_delta.InsertNextTuple(self.displacement_delta[i, :])

            vtk_normal = vtk.vtkFloatArray()
            vtk_normal.SetNumberOfComponents(3)
            # vtk_normal.SetNumberOfValues(self.num_cells)
            vtk_normal.SetName("normal")

            vtk_area = vtk.vtkFloatArray()
            # vtk_area.SetNumberOfComponents(1)
            # vtk_area.SetNumberOfValues(self.num_cells)
            vtk_area.SetName("area")

            for i in range(self.num_cells):
                vtk_normal.InsertNextTuple3(self.normals[i, 0], self.normals[i, 1], self.normals[i, 2])
                vtk_area.InsertNextValue(self.cell_areas[i])

            self.grid.GetCellData().AddArray(vtk_normal)
            self.grid.GetCellData().AddArray(vtk_area)

        else:
            for i in range(self.num_nodes):
                vtk_force.InsertNextTuple(np.append(self.force[i, :], 0.0))
                vtk_displacement.InsertNextTuple(np.append(self.displacement[i, :], 0.0))
                vtk_disp_delta.InsertNextTuple(np.append(self.displacement_delta[i, :], 0.0))

        self.grid.GetPointData().AddArray(vtk_force)
        self.grid.GetPointData().AddArray(vtk_displacement)
        self.grid.GetPointData().AddArray(vtk_disp_delta)

        writer = vtk.vtkXMLUnstructuredGridWriter()
        # writer.SetDataModeToBinary()
        writer.SetFileName(self.vtk_output + '{0:05d}.vtu'.format(self.iteration))
        writer.SetInputData(self.grid)
        # writer.SetDataModeToAscii()
        writer.Write()

        # writer = vtk.vtkUnstructuredGridWriter()
        # writer.SetInputData(grid)
        # writer.SetFileName("./output/Output.vtu")
        # writer.Write()


if __name__ == "__main__":
    main(sys.argv[1:])
