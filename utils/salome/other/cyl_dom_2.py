#!/usr/bin/env python

###
### This file is generated automatically by SALOME v9.3.0 with dump python functionality
###

import sys
import salome

salome.salome_init()
import salome_notebook
notebook = salome_notebook.NoteBook()
sys.path.insert(0, r'C:/Users/claudio.caccia/Dropbox/00_Aero/10_Tesi/salome')

###
### GEOM component
###

import GEOM
from salome.geom import geomBuilder
import math
import SALOMEDS


geompy = geomBuilder.New()

geomObj_1 = geompy.MakeVertex(0, 0, 0)
geomObj_2 = geompy.MakeVectorDXDYDZ(1, 0, 0)
geomObj_3 = geompy.MakeVectorDXDYDZ(0, 1, 0)
geomObj_4 = geompy.MakeVectorDXDYDZ(0, 0, 1)
geomObj_5 = geompy.MakeVertex(-0.2, -0.2, 0)
O = geompy.MakeVertex(0, 0, 0)
OX = geompy.MakeVectorDXDYDZ(1, 0, 0)
OY = geompy.MakeVectorDXDYDZ(0, 1, 0)
OZ = geompy.MakeVectorDXDYDZ(0, 0, 1)
V1 = geompy.MakeVertex(-0.2, -0.2, 0)
V5 = geompy.MakeVertex(0, -0.02, 0)
V2 = geompy.MakeVertex(2.3, -0.2, 0)
V6 = geompy.MakeVertex(0.4, -0.02, 0)
V3 = geompy.MakeVertex(2.3, 0.21, 0)
V7 = geompy.MakeVertex(0.4, 0.02, 0)
V4 = geompy.MakeVertex(-0.2, 0.21, 0)
V8 = geompy.MakeVertex(0, 0.02, 0)
L1 = geompy.MakeLineTwoPnt(V1, V2)
L5 = geompy.MakeLineTwoPnt(V5, V6)
L2 = geompy.MakeLineTwoPnt(V2, V3)
L6 = geompy.MakeLineTwoPnt(V6, V7)
L3 = geompy.MakeLineTwoPnt(V3, V4)
L7 = geompy.MakeLineTwoPnt(V7, V8)
L4 = geompy.MakeLineTwoPnt(V4, V1)
L8 = geompy.MakeLineTwoPnt(V8, V5)
geomObj_6 = geompy.MakeFaceWires([L1, L2, L3, L4], 1)
geomObj_7 = geompy.MakeFaceWires([L5, L6, L7, L8], 1)
O_1 = geompy.MakeVertex(0, 0, 0)
OX_1 = geompy.MakeVectorDXDYDZ(1, 0, 0)
OY_1 = geompy.MakeVectorDXDYDZ(0, 1, 0)
OZ_1 = geompy.MakeVectorDXDYDZ(0, 0, 1)
V1_1 = geompy.MakeVertex(-0.2, -0.2, 0)
V5_1 = geompy.MakeVertex(0, -0.02, 0)
V2_1 = geompy.MakeVertex(2.3, -0.2, 0)
V6_1 = geompy.MakeVertex(0.4, -0.02, 0)
V3_1 = geompy.MakeVertex(2.3, 0.21, 0)
V7_1 = geompy.MakeVertex(0.4, 0.02, 0)
V4_1 = geompy.MakeVertex(-0.2, 0.21, 0)
V8_1 = geompy.MakeVertex(0, 0.02, 0)
L1_1 = geompy.MakeLineTwoPnt(V1_1, V2_1)
L5_1 = geompy.MakeLineTwoPnt(V5_1, V6_1)
L2_1 = geompy.MakeLineTwoPnt(V2_1, V3_1)
L6_1 = geompy.MakeLineTwoPnt(V6_1, V7_1)
L3_1 = geompy.MakeLineTwoPnt(V3_1, V4_1)
L7_1 = geompy.MakeLineTwoPnt(V7_1, V8_1)
L4_1 = geompy.MakeLineTwoPnt(V4_1, V1_1)
L8_1 = geompy.MakeLineTwoPnt(V8_1, V5_1)
geomObj_8 = geompy.MakeFaceWires([L1_1, L2_1, L3_1, L4_1], 1)
geomObj_9 = geompy.MakeFaceWires([L5_1, L6_1, L7_1, L8_1], 1)
Circle_1 = geompy.MakeCircle(None, None, 0.05)
Face_1 = geompy.MakeFaceWires([Circle_1], 1)
geompy.addToStudy( O, 'O' )
geompy.addToStudy( OX, 'OX' )
geompy.addToStudy( OY, 'OY' )
geompy.addToStudy( OZ, 'OZ' )
geompy.addToStudy( V1, 'V1' )
geompy.addToStudy( V5, 'V5' )
geompy.addToStudy( V2, 'V2' )
geompy.addToStudy( V6, 'V6' )
geompy.addToStudy( V3, 'V3' )
geompy.addToStudy( V7, 'V7' )
geompy.addToStudy( V4, 'V4' )
geompy.addToStudy( V8, 'V8' )
geompy.addToStudy( L1, 'L1' )
geompy.addToStudy( L5, 'L5' )
geompy.addToStudy( L2, 'L2' )
geompy.addToStudy( L6, 'L6' )
geompy.addToStudy( L3, 'L3' )
geompy.addToStudy( L7, 'L7' )
geompy.addToStudy( L4, 'L4' )
geompy.addToStudy( L8, 'L8' )
geompy.addToStudy( O_1, 'O' )
geompy.addToStudy( OX_1, 'OX' )
geompy.addToStudy( OY_1, 'OY' )
geompy.addToStudy( OZ_1, 'OZ' )
geompy.addToStudy( V1_1, 'V1' )
geompy.addToStudy( V5_1, 'V5' )
geompy.addToStudy( V2_1, 'V2' )
geompy.addToStudy( V6_1, 'V6' )
geompy.addToStudy( V3_1, 'V3' )
geompy.addToStudy( V7_1, 'V7' )
geompy.addToStudy( V4_1, 'V4' )
geompy.addToStudy( V8_1, 'V8' )
geompy.addToStudy( L1_1, 'L1' )
geompy.addToStudy( L5_1, 'L5' )
geompy.addToStudy( L2_1, 'L2' )
geompy.addToStudy( L6_1, 'L6' )
geompy.addToStudy( L3_1, 'L3' )
geompy.addToStudy( L7_1, 'L7' )
geompy.addToStudy( L4_1, 'L4' )
geompy.addToStudy( L8_1, 'L8' )
geompy.addToStudy( Circle_1, 'Circle_1' )
geompy.addToStudy( Face_1, 'Face_1' )


if salome.sg.hasDesktop():
  salome.sg.updateObjBrowser()
