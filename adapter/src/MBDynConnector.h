/*
 * MBDynConnector.h
 *
 *  Created on: Feb 17, 2020
 *      Author: claudio
 */

#ifndef SRC_MBDYNCONNECTOR_H_
#define SRC_MBDYNCONNECTOR_H_

#include <string>
#include <vector>
#include <iostream>
#include <fstream>
#include <mbcxx.h>

#include <list>

#include <vtkSmartPointer.h>
#include <vtkUnstructuredGrid.h>
#include <vtkCellArray.h>

class MBDynConnector
{
private:
	MBCNodal *mbc;

	unsigned int dim = 3;

	unsigned int meshNodes;
	unsigned int numCells;
	unsigned int connectivity;

	// int writeinterval;
	std::string outputname;
	double Fr[3] = {0.0};
	double Mr[3] = {0.0};
	double *connForces = nullptr;

	double *connCurrentCoords = nullptr;
	double *connCurrentVelocities = nullptr;

	std::vector<int>* cells = nullptr;

	// int *cells = nullptr;

	vtkSmartPointer<vtkPoints> points; // = vtkSmartPointer<vtkPoints>::New();
	vtkSmartPointer<vtkUnstructuredGrid> unstructuredGrid; // = vtkSmartPointer<vtkUnstructuredGrid>::New();
	vtkSmartPointer<vtkCellArray> cellsArray; // = vtkSmartPointer<vtkCellArray>::New();

	void copyForcesToMBDyn(void);

	void initVTK(void);

public:
	// attributes
	bool delta = false;
	double *connCoords = nullptr;
	double *connDisplacements = nullptr;
	double *connDisplacementDeltas = nullptr;
	bool vtkAscii = false;
	// members
	// constructor
	MBDynConnector();
	// initialize MBCNodal
	int initializeMBCNodal(MBCBase::Rot refnoderot, unsigned int nodes, bool labels, MBCBase::Rot rot, bool accelerations);
	// initialize socket
	int initializeSocket(const char *path);
	// negotiate connection
	int negotiate(int timeout, bool verbose, bool dn);

	// run MBDyn instance
	int runMBDynProgram(std::string location, std::vector<std::string> strargs);


	int solve(void);

	void computeDisplacements(void);

	void computeForces(double *vec, double coeff);

	void getConnDisplacements(double *vec);

	void putForces(bool converged);

	int readMesh(const char* filename);

	void setDimensions(int problemDim);

	void setOutputName(std::string name){outputname = name;}

	unsigned int getMeshNodes(void){return meshNodes;}

	void readCoordinates(void);

	void computeResultants(bool print, std::vector<double> root);

	void writeResultants(std::ofstream& file, double c);

	void writeVTK(int step);

	void printVector(int type);

	// added to be able to read connectivity from the adapter
	// to implement nearest-projection

	unsigned int getNumCells(void){return numCells;}

	std::vector<int> getCellVertices(int i){return cells[i];}



	// void setWriteInterval(int val){writeinterval = val;}

	//void copyForcesFromInterface(double *vec);


	// void runSimulation(int itreations);

	// void getCoords(double *vec);

	//void getCurrentCoords(double *vec);


	// void getForces(double *vec);


	//void finalize();
	~MBDynConnector(void);
};


#endif /* SRC_MBDYNCONNECTOR_H_ */
