/*
 * MBDynAdapter.cpp
 *
 *  Created on: Feb 19, 2020
 *      Author: claudio
 */



#include <unistd.h>
#include <cstdlib>
#include <cstdio>
#include <iostream>
#include <fstream>
#include <algorithm> 
#include <cmath>
#include <rapidjson/document.h>
#include <rapidjson/filereadstream.h>

#include "MBDynAdapter.h"


using namespace precice;
using namespace precice::constants;


MBDynAdapter::MBDynAdapter(const char* filename){
	conn = new MBDynConnector();

	// open cofiguration file
	FILE* fp = fopen(filename, "r");

	char readBuffer[65536];
	rapidjson::FileReadStream is(fp, readBuffer, sizeof(readBuffer));

	document.ParseStream(is);

	fclose(fp);

	assert(document.IsObject());
}

int MBDynAdapter::initializeConnector(){


	// set vtk output name
	if(document.HasMember("vtk-output")){
		std::cout << "[MBDyn-adapter] setup file name for VTU output" << std::endl;
		conn->setOutputName(document["vtk-output"].GetString());
	}else{
		std::cout << "[MBDyn-adapter] WARNING: using default file name for VTU output" << std::endl;
		conn->setOutputName("./output/test01_");
	}



	assert(document.HasMember("mesh"));
	if(conn->readMesh(document["mesh"].GetString())){
		std::cerr << "[MBDyn-adapter] Mesh file not read. Exiting..." << std::endl;
		return(EXIT_FAILURE);
	}

	// MBDyn log file location
	std::string location;

	if(document.HasMember("mbdyn-log-location")){
		std::cout << "[MBDyn-adapter] set directory for MBDyn log file..." << std::endl;
		location = document["mbdyn-log-location"].GetString();
		// check if last character is slash
		if(location.back() != '/'){
			location.append("/");
		}

	}else{
		location = "./";
		std::cout << "[MBDyn-adapter] Using current directory as location for MBDyn log file..." << std::endl;
	}



	// vector of strings containing command line arguments to be used to launch mbdyn
	std::vector<std::string> strargs;

	// command
	strargs.push_back("mbdyn");
	// -f option
	strargs.push_back("-f");
	// input filename
	assert(document.HasMember("mbdyn-input"));
	strargs.push_back(document["mbdyn-input"].GetString());
	// -o option
	strargs.push_back("-o");
	// output files root
	assert(document.HasMember("mbdyn-output"));
	strargs.push_back(document["mbdyn-output"].GetString());
	// redirect stderr to stdout
	strargs.push_back("2>&1");

	if(conn->runMBDynProgram(location, strargs)){
		std::cerr << "[MBDyn-adapter] MBDyn run failed. Exiting..." << std::endl;
		return(EXIT_FAILURE);
	}else{
		std::cout << "[MBDyn-adapter] MBDyn started with input file: " << strargs[2] << std::endl;
	}

	// Ext Node parameters (as seen in the example)
	MBCBase::Rot refnoderot = MBCBase::NONE;
	unsigned int nodes = conn->getMeshNodes();
	bool labels = false;
	MBCBase::Rot rot = MBCBase::NONE;
	bool accelerations = false;
	int timeout = -1;
	bool verbose = true;
	bool data_and_next = false;

	if(conn->initializeMBCNodal(refnoderot, nodes, labels, rot, accelerations)){
		std::cerr << "[MBDyn-adapter] Connector initialization failed, Exiting..." << std::endl;
		return(EXIT_FAILURE);
	}

	if(document.HasMember("node-socket")){
		std::cout << "[MBDyn-adapter] Socket defined in config file." << std::endl;
		if(conn->initializeSocket(document["node-socket"].GetString())){
			return(EXIT_FAILURE);
		}
	}else{
		std::cout << "[MDByn-adapter] WARNING: Using default socket" << std::endl;
		if(conn->initializeSocket("/tmp/mbdyn.node.sock")){
			return(EXIT_FAILURE);
		}
	}


	if(conn->negotiate(timeout, verbose, data_and_next)){
		return(EXIT_FAILURE);
	}

	// set output file format
	if(document.HasMember("vtk-ascii")){
		std::cout << "[MBDyn-adapter] setting VTU output format to ";
		conn->vtkAscii = document["vtk-ascii"].GetBool();
		if(conn->vtkAscii){
			std::cout << "ASCII" << std::endl;
		}else{
			std::cout << "binary" << std::endl;
		}
	}else{
		std::cout << "[MBDyn-adapter] defaulting VTU output format to binary" << std::endl;
	}


	if(document["read-coords"].GetBool()){
		// Note: -1 produces a "_init.vtu" file
		conn->writeVTK(-1);
		std::cout << "[MBDyn-adapter] read initial coordinates from MBDyn" << std::endl;
		conn->readCoordinates();
	}


	return(EXIT_SUCCESS);

}


int MBDynAdapter::initializeAdapter(){

	/*********  PreCICE configuration  **********/
	std::cout << "[MBDyn-adapter] Initialize Precice parameters... " << std::endl;


	// name of the PreCICE config name (first argument)
	// std::string configFileName(argv[1]);

	// names of participants, mesh, parameters, etc... (same as PreCICE config file)
	std::string meshName = document["meshName"].GetString();
	std::string readDataName = document["readDataName"].GetString();
	std::string writeDataName = document["writeDataName"].GetString();
	std::string participantName = document["participantName"].GetString();

	if(document.HasMember("displacement-delta")){
		conn->delta = document["displacement-delta"].GetBool();
	}

	// Configure PreCICE interface
	/* interface parameters:
		- name:
		- process index
		- process size

	*/

	assert(document.HasMember("precice-config"));
	// PreCICE 2.0.1 SolverInterface interface(solverName, configFileName, commRank, commSize);
	interface = new SolverInterface(participantName, document["precice-config"].GetString(), 0, 1);
	//SolverInterface interface(participantName, 0, 1);

	std::cout << "[MBDyn-adapter] preCICE configured..." << std::endl;

	// get problem dimension:
	dim = interface->getDimensions();

	std::cout << "[MBDyn-adapter] number of dimensions: " << dim << std::endl;

	// send size to connector
	conn->setDimensions(dim);

	// Build array with structural mesh coordinates:
	//
	// [x0, y0, x1, y1, .... ,x(n-1), y(n-1)]
	// the array is updated each completed step with current coordinate positions
	//
	meshID = interface->getMeshID(meshName);

	std::cout << "[MBDyn-adapter] Mesh ID: " << meshID << std::endl;

	vertexSize = conn->getMeshNodes(); // number of vertices at wet surface

	// determine vertexSize
	vertexIDs =(int *)calloc(vertexSize, sizeof(int));

	// initialize vertex IDs
	for(int i = 0; i < vertexSize; i++){
		vertexIDs[i] = i;
	}

	std::cout << "[MBDyn-adapter] Vertices set." << std::endl;

	adapterCoords = (double *)calloc(dim*vertexSize, sizeof(double));
	adapterDisplacements = (double *)calloc(dim*vertexSize, sizeof(double));
	adapterForces = (double *)calloc(dim*vertexSize, sizeof(double));

	// std::cout << "Memory allocation" << std::endl;

	// copy coordinates from connector, discarding z if problem dimension is 2
	if(dim == 3){
		for (int ii = 0; ii < dim*vertexSize; ++ii) {
			adapterCoords[ii] = conn->connCoords[ii];
		}
	}else{
		for (int ii = 0; ii < vertexSize; ++ii) {
			for (int jj = 0; jj < 2; ++jj) {
				adapterCoords[dim*ii+jj] = conn->connCoords[3*ii+jj];
			}
		}
	}

	// std::cout << "Copy coords 2-3D" << std::endl;


	if(document.HasMember("force-moving-average")){
		if(document["force-moving-average"].GetInt() > 0){
			std::cout << "[MBDyn-adapter] Create Force moving average data" << std::endl;
			ma_force = new MovingAverage(document["force-moving-average"].GetInt(),dim*vertexSize);
		}else{
			std::cerr << "[MBDyn-adapter] Force moving average config not valid. Exiting..." << std::endl;
			exit(1);
		}
		std::cout << "[MBDyn-adapter] NO Force moving average" << std::endl;
		// conn->computeForces(adapterFilteredForces,1.0);
	}

	if(document.HasMember("disp-moving-average")){
		if(document["disp-moving-average"].GetInt() > 0){
			std::cout << "[MBDyn-adapter] Create Displacement moving average data" << std::endl;
			ma_disp = new MovingAverage(document["disp-moving-average"].GetInt(),dim*vertexSize);
		}else{
			std::cerr << "[MBDyn-adapter] Displacement moving average config not valid. Exiting..." << std::endl;
			exit(1);
		}
	}else{
		std::cout << "[MBDyn-adapter] NO Displacement moving average" << std::endl;
	}


	// std::cout << "Moving average" << std::endl;

	// set interface vertices
	interface->setMeshVertices(meshID, vertexSize, adapterCoords, vertexIDs);

	if(document.HasMember("connectivity")){
		if(document["connectivity"].GetBool()){
			std::cout << "[MBDyn-adapter] SET mesh connectivity" << std::endl;

			for (unsigned int curr_id = 0; curr_id < conn->getNumCells(); ++curr_id) {

				std::vector<int> curr_cell = conn->getCellVertices(curr_id);

				int currCellSize = curr_cell.size();

				switch (currCellSize) {
					case 3:
					{
						int edgeIDs[currCellSize];

						for (int curr_vertex = 0; curr_vertex < currCellSize; ++curr_vertex) {
							edgeIDs[curr_vertex] = interface->setMeshEdge(meshID, curr_cell.at(curr_vertex), curr_cell.at((curr_vertex+1)%currCellSize));
						}

						interface->setMeshTriangle(meshID, edgeIDs[0], edgeIDs[1], edgeIDs[2]);
						// or simply use this?
						// interface->setMeshTriangleWithEdges(meshID, i1, i2, i3);


						break;
					}
					case 4:
					{
						int edgeIDs[currCellSize+2];

						edgeIDs[0] = interface->setMeshEdge(meshID, curr_cell.at(0), curr_cell.at(1));
						edgeIDs[1] = interface->setMeshEdge(meshID, curr_cell.at(1), curr_cell.at(2));
						edgeIDs[2] = interface->setMeshEdge(meshID, curr_cell.at(2), curr_cell.at(0));

						edgeIDs[3] = interface->setMeshEdge(meshID, curr_cell.at(0), curr_cell.at(2));
						edgeIDs[4] = interface->setMeshEdge(meshID, curr_cell.at(2), curr_cell.at(3));
						edgeIDs[5] = interface->setMeshEdge(meshID, curr_cell.at(3), curr_cell.at(0));

						// double triangle instead of quad to overcome issues of non planar quad due to MBDyn mapping
						interface->setMeshTriangle(meshID, edgeIDs[0], edgeIDs[1], edgeIDs[2]);
						interface->setMeshTriangle(meshID, edgeIDs[3], edgeIDs[4], edgeIDs[5]);

						// interface->setMeshQuad(meshID, edgeIDs[0], edgeIDs[1],  edgeIDs[2],  edgeIDs[3]);
						// or simply use this?
						// interface->setMeshTriangleWithEdges(meshID, i1, i2, i3);
						break;
					}


					default:
					{
						std::cout << "[MBDyn-adapter] WARNING: cell size " << currCellSize <<  " not valid. Only triangles or quadrangles allowed" << std::endl;
						break;
					}
				}
			}

			// interface->setMeshQuadWithEdges(meshID, i1, i2, i3, i4 );

		}
	}else{
		std::cout << "[MBDyn-adapter] mesh connectivity NOT SET" << std::endl;
	}


	std::cout << "[MBDyn-adapter] Set interface vertices." << std::endl;

	displID = interface->getDataID(readDataName, meshID);
	forceID = interface->getDataID(writeDataName, meshID);

	// solver timestep size
	// maximum PreCICE timestep size
	// TODO: how does it relate to all the timesteps?
	precice_dt = interface->initialize();

	std::cout << "[MBDyn-adapter] interface dt: " << precice_dt << std::endl;

	// should not be necessary:
	conn->computeForces(adapterForces,1.0);
	//std::cout << "Compute forces" << std::endl;

	if(document.HasMember("pre-iteration")){
		for (int ii = 0; ii < document["pre-iteration"].GetInt(); ++ii) {
			conn->solve();
			std::cout << "[MBDyn-adapter] Pre execution number: " << ii << std::endl;
			conn->putForces(true);
			// conn->writeVTK(10000+ii+1);
		}
	}

	// TODO: what if we have displacement deltas?!? we read last delta disp...
	// TODO: now we try not to read displacements
	// conn->getConnDisplacements(adapterDisplacements);

	// Write initial data if required
	if (interface->isActionRequired(actionWriteInitialData())) {
		interface->writeBlockVectorData(displID, vertexSize, vertexIDs, adapterDisplacements);
		interface->markActionFulfilled(actionWriteInitialData());
		std::cout << "[MBDyn-adapter] Initial displacements written!" << std::endl;
		// exit(0);
	}

	// initialize interface data
	interface->initializeData();

	std::cout << "[MBDyn-adapter] Interface data initialized." << std::endl;

	// Read initial data if available
	if (interface->isReadDataAvailable()){
		interface->readBlockVectorData(forceID, vertexSize, vertexIDs, adapterForces);
	}

	conn->writeVTK(0);
	std::cout << "[MBDyn-adapter] Write iteration 0 vtu" << std::endl;

	return(EXIT_SUCCESS);
}

void MBDynAdapter::runSimulation(){
	// define strings used for write and read checkpoints

	int iteration = 0;
	int tmp_it = 0;
	double coeff = 0.0;

	// for step ramp
	double stepsize = 0.0;
	int timesteps = 0;
	double steptime = 0.0;

	bool final_step = false;

	//const std::string& coric = actionReadIterationCheckpoint();
	//const std::string& cowic = actionWriteIterationCheckpoint();


	// retrieve simulation info from json
	assert(document.HasMember("iterstart"));
	int iterstart = document["iterstart"].GetInt();
	assert(document.HasMember("period"));
	int period = document["period"].GetInt();
	assert(document.HasMember("write-interval"));
	int write_interval = document["write-interval"].GetInt();

	std::vector<double> root;

	assert(document["root-coords"].IsArray());
	for (rapidjson::SizeType i = 0; i < document["root-coords"].Size(); i++){ // Uses SizeType instead of size_t
		root.push_back(document["root-coords"][i].GetDouble());
	}

	std::string ramp_string(document["ramp-type"].GetString(), document["ramp-type"].GetStringLength());

	int ramp_type = 0;

	if(ramp_string == "linear"){
		ramp_type = 1;
		std::cout << "[MBDyn-adapter] Using linear progression for fluid forces." << std::endl;
	}else if(ramp_string == "cos"){
		ramp_type = 2;
		std::cout << "[MBDyn-adapter] Using (1-cos) progression for fluid forces." << std::endl;
	}else if(ramp_string == "step"){
		ramp_type = 3;
		std::cout << "[MBDyn-adapter] Using STEP progression for fluid forces." << std::endl;
	}else if(ramp_string == "none"){
		std::cout << "[MBDyn-adapter] NO progression for fluid forces." << std::endl;
	}else{
		std::cout << "[MBDyn-adapter] Progression for fluid forces not parsed. Using none" << std::endl;
	}

	// initial coefficient for forces.
	assert(document.HasMember("coeff0"));
	double coeff0 = document["coeff0"].GetDouble();

	if(ramp_type == 3){
		assert(document.HasMember("step-size"));
		stepsize = document["step-size"].GetDouble();
		timesteps = (int)ceil((1-coeff0)/stepsize);
		steptime = period/(timesteps-1);
	}

	// Create a file to write force at each timestep (used for debug/analysis purposes)
	// open file to write forces
	assert(document.HasMember("resultant-file"));
	std::ofstream resultantFile(document["resultant-file"].GetString());
	resultantFile << "#t Rx Ry Rz Mx My Mz coeff" << std::endl;

	// Start coupling:
	while (interface->isCouplingOngoing()){

		if(interface->isActionRequired(actionWriteIterationCheckpoint())){
			// When an implicit coupling scheme is used, checkpointing is required
			// std::cout << "--- cowic ---" << std::endl;
			// std::cout << cowic << std::endl;
			// stub of function as described in the adapter example
			// saveOldState(); // save checkpoint
			interface->markActionFulfilled(actionWriteIterationCheckpoint());
		}


		// read forces from interface
		if (interface->isReadDataAvailable()) {
			std::cout << "[MBDyn-adapter] DATA to be READ" << std::endl;
			interface->readBlockVectorData(forceID, vertexSize, vertexIDs, adapterForces);
		}


		// copy forces to connector with coefficient
		if(iteration<iterstart){
			coeff = coeff0;
		}else if(iteration <= iterstart + period){
			switch (ramp_type) {
				case 1:{
					coeff = coeff0 + (1.0-coeff0)*(std::min((iteration-iterstart),period)/((double)period));
					break;
				}
				case 2:{
					//std::cout << "----------COS------------" << std::endl;
					coeff = coeff0 + (1.0-coeff0)*(0.5*(1-cos(M_PI*std::min((iteration-iterstart),period)/period)));
					break;
				}
				case 3:{
					int k = (int)ceil((iteration - iterstart)/steptime);
					coeff = std::min(coeff0 + k*stepsize, 1.0);
					break;
				}
				default:{
					coeff = 1.0;
					break;
				}
			}
		}else{
			coeff = 1.0;
		}

		// std::cout << "iteration: " << iteration << " iterstart: " << iterstart << " period: " << period << std::endl;

		if(ramp_type == 3){
			std::cout << "[MBDyn-adapter] forces coefficient: " << coeff << " step size: " << stepsize << " timesteps: " << timesteps << " step time: " << steptime << std::endl;
		}else{
			std::cout << "[MBDyn-adapter] forces coefficient: " << coeff << std::endl;
		}

		if(document.HasMember("force-moving-average")){
			ma_force->getFilteredData(adapterForces);
			conn->computeForces(ma_force->filteredData,coeff);
			// std::cout << "#### Sent filtered forces to MBDyn ####" << std::endl;
		}else{
			conn->computeForces(adapterForces,coeff);
			// std::cout << "#### Sent forces to MBDyn ####" << std::endl;
		}

		// MBDyn compute motion step
		// std::cout << "compute motion step" << std::endl;

		final_step = conn->solve();
		conn->putForces(false);
		final_step = conn->solve();

		// after solving the current time step we make the connector compute the displacement wrt previous time step
		conn->computeDisplacements();

		// the connector computes the resultants on the structure
		conn->computeResultants(true, root);

		// retrieve displacements from connector, which depend on problem dimension
		conn->getConnDisplacements(adapterDisplacements);

		if(interface->isWriteDataRequired(precice_dt)){
			if(document.HasMember("disp-moving-average")){
				std::cout << "[MBDyn-adapter] DATA to be WRITTEN (moving average)" << std::endl;
				ma_disp->getFilteredData(adapterDisplacements);
				interface->writeBlockVectorData(displID, vertexSize, vertexIDs, ma_disp->filteredData);
			}else{
				std::cout << "[MBDyn-adapter] DATA to be WRITTEN (std)" << std::endl;
				// write data to interface
				interface->writeBlockVectorData(displID, vertexSize, vertexIDs, adapterDisplacements);
			}
		}


		// advance
		precice_dt = interface->advance(precice_dt);


		// Send dt to MBDyn
		// std::cout << "send dt (" << precice_dt <<  ") to MBDyn... " << std::endl;
		// ts_socket(sockconn, precice_dt, socksave);


		std::cout << "STEP: " << iteration << " time: " << t << std::endl;

		// Checkpoint
		/*
		if(interface.isActionRequired(cowic)){
			// When an implicit coupling scheme is used, checkpointing is required
			// stub of function as described in the adapter example
			// saveOldState(); // save checkpoint
			std::cout << "--- cowic 2 ---" << std::endl;
			std::cout << cowic << std::endl;
			interface.fulfilledAction(cowic);
		}
		*/

		if(interface->isActionRequired(actionReadIterationCheckpoint())){
			// timestep not converged
			// reloadOldState(); // set variables back to checkpoint
			interface->markActionFulfilled(actionReadIterationCheckpoint());
			std::cout << "---- NOT Converged ----" << std::endl;
			conn->putForces(false);
			if(document.HasMember("every-iteration")){
				if(document["every-iteration"].GetBool()){
					conn->setOutputName("./output/nc_iter_"+std::to_string(iteration)+"_");
					std::cout << "[MBDyn-adapter] EVERY ITERATION" << std::endl;
					conn->writeVTK(tmp_it);
					tmp_it++;
					if(document.HasMember("vtk-output")){
						conn->setOutputName(document["vtk-output"].GetString());
					}else{
						conn->setOutputName("./output/test01_");
					}
				}
			}
		}else{
			// next window
			if(document.HasMember("force-moving-average")){
				ma_force->advance();
			}
			if(document.HasMember("disp-moving-average")){
				ma_disp->advance();
			}

			// timestep converged
			conn->putForces(true);
			tmp_it = 0;
			std::cout << "**** Converged ****" << std::endl;
			// conn->writeVTK(iteration);
			if( ( iteration % write_interval ) == 0 ){
				conn->writeVTK(iteration);
			}

			resultantFile << t << " ";
			conn->writeResultants(resultantFile, coeff);
			iteration++;
			t += precice_dt;
		}

		// end while isCouplingOngoing
		if(final_step){
			std::cout << "[MBDyn-Adapter] MBDyn finished its simulation. Writing last timestep." << std::endl;
			conn->writeVTK(iteration);
			break;
		}
	}

	std::cout << "[MBDyn-adapter] Simulation finished" << std::endl;

}


void MBDynAdapter::finalize(){
	interface->finalize();
	delete conn;
	delete ma_force;
	delete ma_disp;
	std::cout << "[MBDyn-adapter] Finalized" << std::endl;
}


void MBDynAdapter::printVector(int type){

	std::string name;
	double *vec = nullptr;

	switch (type) {
		case 0:{		// coordinates
			vec = adapterCoords;
			name = "ADAPTER Coordinates";
			break;
		}
		case 1:{
			//vec = currentCoords;
			//name = "Current Coordinates";
			std::cout << "ADAPTER current coords not available..." << std::endl;
			return;
			break;
		}
		case 2:{
			vec = adapterDisplacements;
			name = "ADAPTER Displacements";
			break;
		}
		case 3:{
			vec = adapterForces;
			name = "ADAPTER Forces";
			break;
		}
		default:{
			std::cout << "Unrecognized vector type" << std::endl;
			return;
		}
	}
	std::cout << name << std::endl;
	std::cout << "x\t" << "y\t" << "z" << std::endl;
	// for (int n = 0; n < vertexSize; n++) {
	for (int n = 0; n < 10; n++) {
		fprintf(stdout, "%+16.4e %+16.4e %+16.4e\n", vec[3*n], vec[3*n+1], vec[3*n+2]);
	}
}

MBDynAdapter::~MBDynAdapter(void){
	//std::cout << "[MBDyn-adapter] closing tip displacement file." << std::endl;
	//tipDisplacement.close();
	// TODO clean
}


