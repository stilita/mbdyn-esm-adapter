import salome, salome.geom.geomtools

#import GEOM
#from salome.geom import geomBuilder
#geompy = geomBuilder.New(salome.myStudy)

import SMESH  #, SALOMEDS
from salome.smesh import smeshBuilder
smesh = smeshBuilder.New(salome.myStudy)
#from salome.StdMeshers import StdMeshersBuilder

from salome.gui import helper

#import numpy
#import time
#import random
#import ast
#import csv
#import os
#import math


def FindElementType(mesh_dimension, nb_nodes_in_element, boundary = False):
    
    if boundary == True:
        mesh_dimension -= 1
        
    line_type = 3
    triangle_type = 5
    quadrilateral_type = 9
    tetrahedral_type = 10
    hexahedral_type = 12
    wedge_type = 13
    pyramid_type = 14
    
    element_type = None
    
    if mesh_dimension == 1:
        if nb_nodes_in_element == 2:
            element_type = line_type
    elif mesh_dimension == 2:
        if nb_nodes_in_element == 3:
            element_type = triangle_type
        if nb_nodes_in_element == 4:
            element_type = quadrilateral_type
    elif mesh_dimension == 3:
        if nb_nodes_in_element == 4:
            element_type = tetrahedral_type
        if nb_nodes_in_element == 5:
            element_type = pyramid_type
        if nb_nodes_in_element == 6:
            element_type = wedge_type
        if nb_nodes_in_element == 8:
            element_type = hexahedral_type
    else:
        print("mesh dimension not recognized")
        element_type = -1
        
    return element_type


def powerOfTen(figure):
    figure *= 1.0
    n = 0
    
    if figure != 0:
        if abs(figure) < 1:
            while abs(figure) < 1:
                figure *= 10
                n -= 1
                
        if abs(figure) >= 10:
            while abs(figure) >= 10:
                figure /= 10
                n += 1
                
    return figure, n


def ExportSU2File( mesh, file = None, only = [None], ignore = [None]):
    """
	
	
Description:
	Exports a mesh into an .su2 file readable by the CFD solver SU2
	

Arguments:
	# mesh 
		Description:       The mesh to export. 
		Type:              Mesh 
		GUI selection:     yes 
		Selection by name: yes 
		Recursive:         - 
		Default value:     None  

	# file 
		Description:       The name without extension of the amsh file to write. If equals None, the name of the mesh in the study tree is taken. 
		Type:              String 
		GUI selection:     - 
		Selection by name: - 
		Recursive:         - 
		Default value:     None  

	# only 
		Description:       The list of names of groups to export, excluding the others. 
		Type:              List of Strings 
		GUI selection:     - 
		Selection by name: - 
		Recursive:         - 
		Default value:     [None]  

	# ignore 
		Description:       The list of names of groups to ignore. 
		Type:              List of Strings 
		GUI selection:     - 
		Selection by name: - 
		Recursive:         - 
		Default value:     [None]  

Returned Values:
	"dim" value:    - 
	"single" value: - 
	Type:           - 
	Number:         - 
	Name:           -  

Conditions of use:
	The mesh has to be computed and to contain groups describing the desired boundary conditions (inlet, outlet, wall, farfield, etc.).
	

    """

    myMesh_ref = helper.getSObjectSelected()[0].GetObject()
    mesh = smesh.Mesh(myMesh_ref)
    
    # Check the input shape existence
    
    if "error" in [mesh] or None in [mesh]:
        return
    else:
        print("Mesh found")
        
    
    # Get the mesh name
    mesh_name = mesh.GetName()
    print("Mesh name: "+mesh_name)
    
    # Renumber elements and nodes
    mesh.RenumberNodes()
    mesh.RenumberElements()
    
    # Get nodes number and IDs
    nb_nodes_in_mesh = mesh.NbNodes()
    node_ids_in_mesh = mesh.GetNodesId()
    
    # Get edges IDs
    edge_ids_in_mesh = mesh.GetElementsByType(SMESH.EDGE)
    nb_edges_in_mesh = mesh.NbEdges()
    
    # Get faces IDs
    face_ids_in_mesh = mesh.GetElementsByType(SMESH.FACE)
    
    nb_faces_in_mesh = mesh.NbFaces()
    nb_triangles_in_mesh = mesh.NbTriangles()
    nb_quadrangles_in_mesh = mesh.NbQuadrangles()
    
    # Get volumes IDs
    nb_volumes_in_mesh = mesh.NbVolumes()
    
    nb_tetrahedrons_in_mesh = mesh.NbTetras()
    nb_pyramids_in_mesh = mesh.NbPyramids()
    nb_prisms_in_mesh = mesh.NbPrisms()
    nb_hexahedrons_in_mesh = mesh.NbHexas()
    
    volume_ids_in_mesh = mesh.GetElementsByType(SMESH.VOLUME)
    
    # Get mesh dimension
    if nb_volumes_in_mesh != 0:
        mesh_dimension = 3
        nb_elements_in_domain = nb_volumes_in_mesh
    else:
        mesh_dimension = 2
        nb_elements_in_domain = nb_faces_in_mesh
        
    # Get groups
    group_names = mesh.GetGroupNames()
    groups = mesh.GetGroups()
    
    # Sort groups
    sorted_groups = []
    
    if only != [None]:
        for group in groups:
            group_name = group.GetName()
            if group_name in only:
                sorted_groups.append(group)
        groups = sorted_groups
        
    sorted_groups = []
    
    if ignore != [None]:
        for group in groups:
            group_name = group.GetName()
            if group_name not in ignore:
                sorted_groups.append(group)
        groups = sorted_groups
    
    # Get the number of groups
    nb_groups = len(groups)
    
    # Get group types
    
    group_types = []
    
    for group in groups:
        group_type = str(group.GetType())
        group_types.append(group_type)
        
################################
    # Open the su2 file
    
    if file == None:
        file = mesh_name
        
    su2_file = open("%s.su2"%(file), "w")
    su2_file.write("NDIME= %i\n"%(mesh_dimension))
    
    # Write the domain element definitions
    print( "[i] Writing definition of domain elements... (%s elements)"%(nb_elements_in_domain))
    
    su2_file.write("NELEM= %i\n"%(nb_elements_in_domain))
    
    if mesh_dimension == 2:
        element_ids_in_domain = face_ids_in_mesh
    elif mesh_dimension == 3:
        element_ids_in_domain = volume_ids_in_mesh
        
    for element_id_in_domain in element_ids_in_domain:
        nb_nodes_in_element = mesh.GetElemNbNodes(element_id_in_domain)
        element_type = FindElementType(mesh_dimension, nb_nodes_in_element)
        element_definition = str(element_type)
        
        for n in range(nb_nodes_in_element):
            
            #if element_type == pyramid_type: n *= -1
            node_id = mesh.GetElemNodes(element_id_in_domain)[n]
            node_id -= 1
            element_definition += "\t" + str(node_id)
            
        element_definition += "\t" + str(element_id_in_domain)
        su2_file.write(element_definition + "\n")
        
    
    print( "[i] Writing node coordinates... (%s nodes)"%(nb_nodes_in_mesh))
    
    su2_file.write("NPOIN= %i\n"%(nb_nodes_in_mesh))
    
    # Write the node coordinates
    
    for node_id in node_ids_in_mesh:
        node_definition = ""
        
        for n in range(mesh_dimension):            
            node_coordinate = mesh.GetNodeXYZ(node_id)[n]
            [node_float_coordinate, node_coordinate_power_of_ten] = powerOfTen(node_coordinate)
            node_coordinate = "%.16fE%i"%(node_float_coordinate, node_coordinate_power_of_ten)
            node_definition += "\t" + node_coordinate
            
        node_id -= 1
        node_definition += "\t" + str(node_id)
        su2_file.write(node_definition + "\n")
        
    # Get the group element definition
    
    print( "[i] Writing definition of group elements... (%s groups)"%(nb_groups))
    
    su2_file.write("NMARK= %i\n"%(nb_groups))
    
    for group in groups:# For each group of the mesh
        group_name = group.GetName()
        su2_file.write("MARKER_TAG= %s\n"%(group_name))
        element_ids_in_group = group.GetListOfID()
        nb_elements_in_group = len(element_ids_in_group)
        su2_file.write("MARKER_ELEMS= %s\n"%(nb_elements_in_group))
        
        for element_id_in_group in element_ids_in_group:
            nb_nodes_in_element = mesh.GetElemNbNodes(element_id_in_group)
            element_type = FindElementType(mesh_dimension, nb_nodes_in_element, boundary = True)
            element_definition = str(element_type)
            
            for n in range(nb_nodes_in_element):
                node_id = mesh.GetElemNodes(element_id_in_group)[n]
                node_id -= 1
                element_definition += "\t" + str(node_id)
                
            element_definition += "\t" + str(element_id_in_group)
            su2_file.write(element_definition + "\n")
            
    # Close the files
    su2_file.close()    		    