/*
 * Adapter.cpp
 *
 *  Created on: Feb 17, 2020
 *      Author: claudio
 */


#include <cstdlib>
#include <cstdio>
#include <iostream>
#include <unistd.h>

#include "MBDynConnector.h"
#include "MBDynAdapter.h"
// #include "localfunctions.h"

int main(int argc, char **argv) {

	int c;
	const char* jsonfile = nullptr;

    while( ( c = getopt (argc, argv, "f:") ) != -1 )
    {
        switch(c)
        {
            case 'f':
                if(optarg){
                	jsonfile = optarg;
                }
                std::cout << "Using " << jsonfile << " configuration file" << std::endl;
                break;
            /*
            case 't':
                if(optarg) tvalue = std::atoi(optarg) ;
                break;
            */
            default:
            	std::cerr << "Argument not recognized. Exiting..." << std::endl;
            	exit(EXIT_FAILURE);
        }
    }

    if(!jsonfile){
    	std::cerr << "No JSON configuration file. Exiting..." << std::endl;
    	exit(EXIT_FAILURE);
    }

	MBDynAdapter *adapter = new MBDynAdapter(jsonfile);


	if(adapter->initializeConnector()){
		std::cerr << "Connector not Initialized. Exiting..." << std::endl;
		exit(EXIT_FAILURE);
	}

	// std::cout << "Nodes: " << adapter->conn->getMeshNodes() << std::endl;

	adapter->initializeAdapter();

	adapter->runSimulation();

	adapter->finalize();

	//conn->setWriteInterval(20);
	//conn->setOutputName("./output/test_");

	//conn->runSimulation(2001);


	exit(EXIT_SUCCESS);
}
