#import re
import numpy as np
# import warnings
# warnings.filterwarnings("error")

def read_of_force(filename='force.dat'):
    #forceRegex=r"([0-9.Ee\-+]+)\s+\(+([0-9.Ee\-+]+)\s([0-9.Ee\-+]+)\s([0-9.Ee\-+]+)\)\s\(([0-9.Ee\-+]+)\s([0-9.Ee\-+]+)\s([0-9.Ee\-+]+)\)+\s\(+([0-9.Ee\-+]+)\s([0-9.Ee\-+]+)\s([0-9.Ee\-+]+)\)\s\(([0-9.Ee\-+]+)\s([0-9.Ee\-+]+)\s([0-9.Ee\-+]+)\)+"
    t = []
    # fpx = []; fpy = []; fpz = []
    # fvx = []; fvy = []; fvz = []
    # mpx = []; mpy = []; mpz = []
    # mvx = []; mvy = []; mvz = []
    f_dict = {}
    pipefile=open(filename,'r')
    lines = pipefile.readlines()
    for line in lines:
        if not line.startswith('#'):
            #ch0 = re.split(r"\s{2,}", line)
            # split on first tab cfr problem with long float time
            ch0 = line.split('\t',1)
            #print(ch0)
            time = float(ch0[0])
            forces = ch0[1].split('\t')
            f_dict[time] = np.fromstring(forces[0][1:-1],sep=' ')
            f_dict[time] = np.append(f_dict[time], np.fromstring(forces[1][1:-1],sep=' '))
            f_dict[time] = np.append(f_dict[time], np.fromstring(forces[2][1:-2],sep=' '))
    t = np.fromiter(f_dict.keys(), dtype=float)
    
    data = np.zeros((t.size,9))
    
    for i,act_t in enumerate(t):
        #print(i,act_t)
        data[i,:] = f_dict[act_t]
    
    # print(data)
    pipefile.close()
    
    return t, data
            

def read_struct_force(filename='struct_force.dat'):
    return np.genfromtxt(filename)

if __name__ == "__main__":
    read_of_force()