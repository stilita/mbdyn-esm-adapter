/*
 * MovingAverage.cpp
 *
 *  Created on: Mar 16, 2020
 *      Author: claudio
 */

#include "MovingAverage.h"
#include <cstdlib>

// constructor

MovingAverage::MovingAverage(int window, int rows){
	windowSize = window;

	coeff = 1.0 / (double)window;

	rowsDim = rows;

	filteredData = (double *)calloc(rowsDim, sizeof(double));

	data = (double **)calloc(windowSize ,sizeof(double *));

	for (int ii = 0; ii < windowSize; ++ii) {
		data[ii] = (double *)calloc(rowsDim, sizeof(double));
	}
}

void MovingAverage::getFilteredData(const double *currentData){

	for (int ii = 0; ii < rowsDim; ++ii) {
		filteredData[ii] = filteredData[ii] + coeff*(currentData[ii] - data[current][ii]);
		data[current][ii] = currentData[ii];
	}
}

void MovingAverage::advance(void){
	current = (current+1) % windowSize;
}

MovingAverage::~MovingAverage(){

	for (int ii = 0; ii < windowSize; ++ii) {
		free(data[ii]);
	}
	free(data);

}
