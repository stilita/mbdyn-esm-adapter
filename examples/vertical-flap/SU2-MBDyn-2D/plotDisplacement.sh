#! /bin/bash
gnuplot -p << EOF
set grid
set title 'Displacement of the Flap Tip'
set xlabel 'Time [s]'
set ylabel 'Y-Displacement [m]'
plot "./MBDyn/precice-MBDyn-watchpoint-tip.log" using 1:4 with lines title ""
EOF

