#!/usr/bin/env python

###
### This file is generated automatically by SALOME v9.3.0 with dump python functionality
###

import sys
import salome

import numpy as np

salome.salome_init()
import salome_notebook
notebook = salome_notebook.NoteBook()
sys.path.insert(0, r'C:/Users/claudio.caccia/Dropbox/00_Aero/10_Tesi/salome')

###
### GEOM component
###

import GEOM
from salome.geom import geomBuilder
import math
import SALOMEDS


geompy = geomBuilder.New()

O = geompy.MakeVertex(0, 0, 0)
OX = geompy.MakeVectorDXDYDZ(1, 0, 0)
OY = geompy.MakeVectorDXDYDZ(0, 1, 0)
OZ = geompy.MakeVectorDXDYDZ(0, 0, 1)

H = 0.2
d = 0.01
R = 0.05
L = 2.5
t = 0.04
ll = 0.4

x_est = np.array([-H, L-H, L-H, -H])
y_est = np.array([-H, -H, H+d, H+d])


x_lama = np.array([0, ll, ll, 0])
y_lama = np.array([-1, -1, 1, 1])*0.5*t


v_est = []
v_lama = []

for i in range(4):
	v_est.append(geompy.MakeVertex(x_est[i], y_est[i], 0))
	v_lama.append(geompy.MakeVertex(x_lama[i], y_lama[i], 0))

l_est = []
l_lama = []

for i in range(4):
	l_est.append(geompy.MakeLineTwoPnt(v_est[i], v_est[(i+1)%4]))
	l_lama.append(geompy.MakeLineTwoPnt(v_lama[i], v_lama[(i+1)%4]))


Circle_1 = geompy.MakeCircle(None, None, 0.05)
Face_1 = geompy.MakeFaceWires(l_est, 1)
Face_2 = geompy.MakeFaceWires([Circle_1], 1)
Face_3 = geompy.MakeFaceWires(l_lama, 1)
Fuse_1 = geompy.MakeFuseList([Face_2, Face_3], True, True)
Cut_1 = geompy.MakeCutList(Face_1, [Fuse_1], True)




geompy.addToStudy( O, 'O' )
geompy.addToStudy( OX, 'OX' )
geompy.addToStudy( OY, 'OY' )
geompy.addToStudy( OZ, 'OZ' )


for i in range(4):
	geompy.addToStudy( v_est[i], 'V'+str(i+1) )


for i in range(4):
	geompy.addToStudy( v_lama[i], 'V'+str(i+5) )


for i in range(4):
	geompy.addToStudy( l_est[i], 'L'+str(i+1) )

for i in range(4):
	geompy.addToStudy( l_lama[i], 'L'+str(i+5) )

geompy.addToStudy( Cut_1, 'domain' )

if salome.sg.hasDesktop():
  salome.sg.updateObjBrowser()
