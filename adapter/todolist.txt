Setup:
- mbdyn 1.7.3 python 2 (?) pb con ext structural mapping (n_x) in python
- precice 1.5.2 -> 2.0 no retrocompatibilità.
- openfoam 7: caso flap
- SU2 in standby: adapter alla versione 6, SU2 7, no retrocompatibilità, incompressible
- Ubuntu 18.04. pb con versione PETSC e MPI -> 20.04

Modello:
- tutorial FSI precice, lato fluid OF7 identico.
- mbdyn sostituisce Calculix. 5 elementi beam3 (al momento non configurabile)
- il pb è sostanzialmente 2D (mesh cmq 3D).
- come si comporterebbe mbdyn con una mesh 2D?
- uso la stessa mesh di interfaccia fluido per i punti del mapping (uso mapping precice più semplici)
- dat file analogo a "nuvola grid", usato anche nel programma
- a che serve la connettività nel file dat per lo structural mapping? (singolo/multiplo)
- salvataggio dei risultati (forze e spostamenti nodali ai punti del mapping in un file vtu)

- esiste un benchmark SU2 fatto in 2D incomprimibile. limitazioni per adapter, per mbdyn?
- importanza del posizionamento dei 3 punti ad ogni nodo

Esecuzione:
- momenti d'inerzia esagerati in torsione, taglio z e flessione y per mantenere il pb bidimensionale
  (pb nell'esecuzione forse rientrati: mi aspetterei forze nodali simmetriche rispetto a xy, in effetti è così, ma a volte all'inizio diverge torcendosi)
- il pb con i dati del tutorial precice divergono subito. Riesce a partire con E x1e3 e viscosità elevata
- lo stesso fenomeno avviene con il solo mbdyn e mapping con carico step
- ordine esecuzione: GetMotion compute forces and displacements, PutForces
- soluzione rumorosa

Struttura:
- 2 classi: una di connessione ad mbdyn + esecuzione programma, l'altra di adapter con precice
- external socket per timestamp? (dipende dal tipo di esecuzione: serial/parallel implicit/explicit?) esistono limitazioni con mapping?

Configurabilità:
- trave incastrata rettilinea orientamento qualsiasi, n° elementi qualsiasi equidistribuiti. elementi body a +/-1/2 del nodo centrale
- estensioni? 
