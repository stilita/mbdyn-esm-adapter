#!/usr/bin/env python

###
### This file is generated automatically by SALOME v9.3.0 with dump python functionality
###

import sys
import salome

salome.salome_init()
import salome_notebook
notebook = salome_notebook.NoteBook()
sys.path.insert(0, r'C:/Users/claudio.caccia/Dropbox/00_Aero/10_Tesi/salome')

###
### GEOM component
###

import GEOM
from salome.geom import geomBuilder
import math
import SALOMEDS


geompy = geomBuilder.New()

O = geompy.MakeVertex(0, 0, 0)
OX = geompy.MakeVectorDXDYDZ(1, 0, 0)
OY = geompy.MakeVectorDXDYDZ(0, 1, 0)
OZ = geompy.MakeVectorDXDYDZ(0, 0, 1)

H = 0.01
L = 0.04
t = 0.06*1e-2

V1 = geompy.MakeVertex(-5*H, -3*H, 0)
V2 = geompy.MakeVertex(14.5*H, -3*H, 0)
V3 = geompy.MakeVertex(14.5*H, 3*H, 0)
V4 = geompy.MakeVertex(-5*H, 3*H, 0)

L1 = geompy.MakeLineTwoPnt(V1, V2)
L2 = geompy.MakeLineTwoPnt(V2, V3)
L3 = geompy.MakeLineTwoPnt(V3, V4)
L4 = geompy.MakeLineTwoPnt(V4, V1)



geompy.addToStudy( O, 'O' )
geompy.addToStudy( OX, 'OX' )
geompy.addToStudy( OY, 'OY' )
geompy.addToStudy( OZ, 'OZ' )

geompy.addToStudy( V1, 'V1' )
geompy.addToStudy( V2, 'V2' )
geompy.addToStudy( V3, 'V3' )
geompy.addToStudy( V4, 'V4' )

geompy.addToStudy( L1, 'L1' )
geompy.addToStudy( L2, 'L2' )
geompy.addToStudy( L3, 'L3' )
geompy.addToStudy( L4, 'L4' )



if salome.sg.hasDesktop():
  salome.sg.updateObjBrowser()
