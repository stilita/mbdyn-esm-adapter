import getopt
import json
import sys

import numpy as np
# from mpi4py import MPI
import precice
import vtk


def main(argv):
    try:
        opts, args = getopt.getopt(argv,"hf:",["file="])
    except getopt.GetoptError:
        print('fluid-stub.py -f <inputfile.json>')
        sys.exit(2)
    for opt, arg in opts:
        if opt == '-h':
            print('fluid-stub.py -f <inputfile.json>')
            sys.exit()
        elif opt in ("-f", "--file"):
            inputfile = arg

    np.set_printoptions(precision=7)

    with open(inputfile, 'r') as f:
        config = json.load(f)

    fluid = FluidStub(config)
    fluid.run()


class FluidStub:
    def __init__(self, config):
        #interface = precice.Interface(solverName, configFileName, processRank, processSize)
        self.interface = precice.Interface(config["fluidParticipantName"], config["precice-config"], 0, 1) # proc no, nprocs
        self.dim = self.interface.get_dimensions()
        self.num_nodes = 0

        self.read_mesh(config["mesh"])

        # self.compute_areas()
        self.delta = config["displacement-is_delta"]
        self.wrt_interval = config["write-interval"]

        nmeshID = self.interface.get_mesh_id(config["fluidMeshName"])

        self.nodeVertexIDs = self.interface.set_mesh_vertices(nmeshID, self.nodes)

        self.force = np.zeros((self.num_nodes,self.dim))
        # total displacement
        self.displacement = np.zeros_like(self.force)
        # is_delta displacement from 2 iterations
        self.displacement_delta = np.zeros_like(self.force)
        # data from interface
        self.disp_from_interface = np.zeros_like(self.force)


        self.displacementID = self.interface.get_data_id(config["readDataName"], nmeshID)
        self.forceID = self.interface.get_data_id(config["writeDataName"], nmeshID)

        self.dt = self.interface.initialize()

        if self.interface.is_action_required(precice.action_write_initial_data()):
            self.interface.write_block_vector_data(self.forceID, self.nodeVertexIDs, self.force)
            self.interface.mark_action_fulfilled(precice.action_write_initial_data())

        self.interface.initialize_data()

        if self.interface.is_read_data_available():
            self.disp_from_interface = self.interface.read_block_vector_data(self.displacementID, self.nodeVertexIDs)
            self.displacement = self.disp_from_interface.copy()
            self.displacement_delta = self.disp_from_interface.copy()
            self.coords =  self.displacement + self.nodes.copy()
            print("data available")
        else:
            print("NO data available")
            self.coords = self.nodes.copy()

        if self.dim == 3:
            self.compute_areas_and_normals()

    def read_mesh(self, filename):
        with open(filename, 'r') as mf:

            print("Read mesh")
            r = mf.readline()

            while r[0] == '#':
                r = mf.readline()

            try:
                self.num_nodes = int(r)
            except ValueError as ve:
                print(ve)
                print("Number of nodes not found!")
                print("Extiting...")
                return

            self.nodes = np.zeros((self.num_nodes, self.dim))

            for i in range(self.num_nodes):
                point = mf.readline().split()
                if self.dim < len(point):
                    self.nodes[i, :] = [float(coord) for coord in point[0:self.dim]]
                else:
                    self.nodes[i, :] = [float(coord) for coord in point]

            # read number of cells
            r = mf.readline()
            try:
                self.num_cells = int(r)
            except ValueError as ve:
                print(ve)
                print("Number of cells not found!")
                print("Extiting...")
                return

            self.cells = np.zeros((self.num_cells, 3),dtype=np.int16)

            if self.dim == 3:
                self.cell_areas = np.zeros(self.num_cells)
                self.normals = np.zeros((self.num_cells, 3))

            for i in range(self.num_cells):
                connectivity = mf.readline().split()
                self.cells[i, :] = [int(node)-1 for node in connectivity]

        print("Mesh read")
        # print(self.nodes[0,:])
        # print(self.nodes[1, :])
        # print(self.nodes[14, :])
        # print(self.nodes[23, :])
        # print(self.nodes[12,:])
        # print(self.nodes[4, :])
        # print(self.nodes[5, :])
        # print(self.nodes[13, :])


    def compute_areas_and_normals(self):

        for i in range(self.num_cells):
            point_a = self.coords[self.cells[i,0],:]
            point_b = self.coords[self.cells[i,1],:]
            point_c = self.coords[self.cells[i,2],:]

            segm_ab = point_b - point_a
            segm_bc = point_c - point_b

            self.cell_areas[i] = np.linalg.norm(segm_ab)*np.linalg.norm(segm_bc)
            tempv = np.cross(segm_ab,segm_bc)
            self.normals[i,:] = tempv/np.linalg.norm(tempv)

            # if i==44 or i==45:
            #     print("----AREA----")
            #     print("Point A: x:{0} y:{1} z{2}".format(point_a[0],point_a[1],point_a[2]))
            #     print("Point B: x:{0} y:{1} z{2}".format(point_b[0],point_b[1],point_b[2]))
            #     print("Point C: x:{0} y:{1} z{2}".format(point_c[0],point_c[1],point_c[2]))
            #     print("Segmento AB: {0} - lunghezza: {1}".format(segm_ab,np.linalg.norm(segm_ab)))
            #     print("Segmento BC: {0} - lunghezza: {1}".format(segm_bc,np.linalg.norm(segm_bc)))
            #     print("Normale: x:{0} y:{1} z:{2}".format(self.normals[i,0],self.normals[i,1],self.normals[i,2]))
            #     print("AREA: {0}".format(self.cell_areas[i]))


        print("normals computed. NB into the solid")
        # print(self.cell_areas[44:46])
        # print(self.normals)
        # print(self.nodes)
        # print(self.cells)
        #print(self.coords)
        #print(pippo)

    def compute_areas(self):
        self.cell_areas = np.zeros(self.num_cells)

        for i in range(self.num_cells):
            for k in range(4):
                #x1z2-z1x2
                self.cell_areas[i] += (self.nodes[self.cells[i,k],0]*self.nodes[self.cells[i,(k+1)%4],2] - self.nodes[self.cells[i,(k+1)%4],0]*self.nodes[self.cells[i,k],2])

        self.cell_areas = 0.5*np.abs(self.cell_areas)

    def set_forces_2D(self):
        self.force.fill(0)

        self.force[1,1] = 0.0
        self.force[2,1] = 0.0

    def set_nodal_force(self):

        self.force.fill(0)

        f_nodal = 0.0

        # axial
        self.force[96,0] = f_nodal
        self.force[97,0] = f_nodal
        print("nodal")

    def set_forces(self):

        # if self.iteration < 10:
        #     coeff = 0.0
        # else:
        #     coeff = min((self.iteration-10)/500., 1.0)
        #
        # f_nodal = .1
        #
        # # axial
        # self.force[96,1] = coeff*f_nodal
        # self.force[97,1] = coeff*f_nodal

        self.force.fill(0)

        pressure = 0.001

        for i in range(self.num_cells):

            # consider only upper cells, with negative y normal component
            if self.normals[i, 1] < -0.0 and self.normals[i, 0] <= 0.0:
                ftemp = pressure*self.normals[i,:]*self.cell_areas[i]/4.0

                for k in range(3):
                    self.force[self.cells[i,k],:] += ftemp

        Ftot = np.sum(self.force, axis=1)
        Fmag = np.sqrt(np.sum(Ftot**2))
        print("Fx: {0} Fy: {1} Fz: {2} - mag: {3}".format(Ftot[0], Ftot[1], Ftot[2], Fmag))

    def run(self):
        self.iteration = 0

        self.write_vtk()
        t = 0.0

        while self.interface.is_coupling_ongoing():
            if self.interface.is_action_required(precice.action_write_iteration_checkpoint()):
                self.interface.mark_action_fulfilled(precice.action_write_iteration_checkpoint())

            self.disp_from_interface = self.interface.read_block_vector_data(self.displacementID, self.nodeVertexIDs)

            if self.dim == 3:
                self.compute_areas_and_normals()
                # self.set_nodal_force()
                self.set_forces()
            else:
                self.set_forces_2D()

            self.interface.write_block_vector_data(self.forceID, self.nodeVertexIDs, self.force)
            self.interface.advance(self.dt)

            # if self.iteration > 11:
            #     print("FORCING STOP...")
            #     break

            if self.interface.is_action_required(precice.action_read_iteration_checkpoint()): # i.e. not yet converged
                self.interface.mark_action_fulfilled(precice.action_read_iteration_checkpoint())
            else:
                if self.delta:
                    self.displacement_delta = self.disp_from_interface.copy()
                    self.displacement += self.displacement_delta
                else:
                    self.displacement_delta = self.disp_from_interface - self.displacement
                    self.displacement = self.disp_from_interface.copy()

                self.coords = self.nodes + self.displacement


                #if self.iteration % 10 == 0:
                self.iteration += 1
                if self.iteration % self.wrt_interval == 0:
                    self.write_vtk()
                t += self.dt

                #print(np.reshape(self.displacement,(-1,3))[:20,:])
                #print(self.coords[:20,:])

                print("iteration {}".format(self.iteration))

    def write_vtk(self):

        grid = vtk.vtkUnstructuredGrid()
        grid.Allocate(1, 1)

        points = vtk.vtkPoints()
        points.SetNumberOfPoints(self.num_nodes)

        for i in range(self.num_nodes):
            if self.dim == 3:
                points.InsertPoint(i,self.nodes[i,:])
            else:
                points.InsertPoint(i, np.append(self.nodes[i, :],0.0))

        grid.SetPoints(points)

        for i in range(self.num_cells):
            quad = vtk.vtkTriangle()
            for k in range(3):
                quad.GetPointIds().SetId(k, self.cells[i,k])

            grid.InsertNextCell(quad.GetCellType(), quad.GetPointIds())

        vtk_force = vtk.vtkDoubleArray()
        vtk_force.SetNumberOfComponents(3)
        # vtk_force.SetNumberOfValues(self.num_nodes)
        vtk_force.SetName("force")

        vtk_displacement = vtk.vtkDoubleArray()
        vtk_displacement.SetNumberOfComponents(3)
        # vtk_displacement.SetNumberOfValues(self.num_nodes)
        vtk_displacement.SetName("displacement")

        vtk_disp_delta = vtk.vtkDoubleArray()
        vtk_disp_delta.SetNumberOfComponents(3)
        # vtk_displacement.SetNumberOfValues(self.num_nodes)
        vtk_disp_delta.SetName("displacement-is_delta")

        if self.dim == 3:
            for i in range(self.num_nodes):
                vtk_force.InsertNextTuple(self.force[i,:])
                vtk_displacement.InsertNextTuple(self.displacement[i,:])
                vtk_disp_delta.InsertNextTuple(self.displacement_delta[i,:])

            vtk_normal = vtk.vtkFloatArray()
            vtk_normal.SetNumberOfComponents(3)
            # vtk_normal.SetNumberOfValues(self.num_cells)
            vtk_normal.SetName("normal")

            vtk_area = vtk.vtkFloatArray()
            #vtk_area.SetNumberOfComponents(1)
            # vtk_area.SetNumberOfValues(self.num_cells)
            vtk_area.SetName("area")

            for i in range(self.num_cells):
                vtk_normal.InsertNextTuple3(self.normals[i,0],self.normals[i,1],self.normals[i,2])
                vtk_area.InsertNextValue(self.cell_areas[i])

            grid.GetCellData().AddArray(vtk_normal)
            grid.GetCellData().AddArray(vtk_area)

        else:
            for i in range(self.num_nodes):
                vtk_force.InsertNextTuple(np.append(self.force[i,:],0.0))
                vtk_displacement.InsertNextTuple(np.append(self.displacement[i,:],0.0))
                vtk_disp_delta.InsertNextTuple(np.append(self.displacement_delta[i,:],0.0))


        grid.GetPointData().AddArray(vtk_force)
        grid.GetPointData().AddArray(vtk_displacement)
        grid.GetPointData().AddArray(vtk_disp_delta)

        writer = vtk.vtkXMLUnstructuredGridWriter()
        #writer.SetDataModeToBinary()
        writer.SetFileName('./output/stub_{0:05d}.vtu'.format(self.iteration))
        writer.SetInputData(grid)
        # writer.SetDataModeToAscii()
        writer.Write()

        # writer = vtk.vtkUnstructuredGridWriter()
        # writer.SetInputData(grid)
        # writer.SetFileName("./output/Output.vtu")
        # writer.Write()


if __name__ == "__main__":
    main(sys.argv[1:])
