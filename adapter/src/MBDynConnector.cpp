/*
 * MBDynConnector.cpp
 *
 *  Created on: Feb 17, 2020
 *      Author: claudio
 */

#include <cstdio>
#include <cstdlib>
#include <iostream>

// used to create variable connectivity
#include <list>
#include <iterator>

// used to parse string of integers
#include <sstream>

#include <unistd.h>
#include <fcntl.h>
#include <algorithm>
#include <fstream>
#include <cstring>
#include <cmath>

#include <mbcxx.h>

#include <vtkSmartPointer.h>
#include <vtkQuad.h>
#include <vtkTriangle.h>
#include <vtkPolygon.h>
#include <vtkDoubleArray.h>
//#include <vtkTetra.h>
#include <vtkCellArray.h>
#include <vtkXMLUnstructuredGridReader.h>
#include <vtkDataSetMapper.h>
#include <vtkActor.h>
//#include <vtk-7.1/vtkRenderer.h>
//#include <vtk-7.1/vtkRenderWindow.h>
//#include <vtk-7.1/vtkRenderWindowInteractor.h>
#include <vtkXMLUnstructuredGridWriter.h>
#include <vtkUnstructuredGrid.h>
#include <vtkPointData.h>
#include <vtkVertexGlyphFilter.h>
#include <vtkDataSetAttributes.h>




#include "MBDynConnector.h"

MBDynConnector::MBDynConnector(){
	meshNodes = 0;
	numCells = 0;
	connectivity = 0;
	mbc = new MBCNodal;
	// writeinterval = 10;
}

int MBDynConnector::initializeMBCNodal(MBCBase::Rot refnoderot,
		unsigned int nodes, bool labels, MBCBase::Rot rot, bool accelerations){
	meshNodes = nodes;
	if(mbc->Initialize(refnoderot, nodes, labels, rot, accelerations)){
		std::cerr << "[MBDyn-connector] MBCNodal::Initialize() failed" << std::endl;
		return(EXIT_FAILURE);
	}
	return(EXIT_SUCCESS);
}

int MBDynConnector::initializeSocket(const char *path){
	if(mbc->Init(path)){
		std::cerr << "[MBDyn-connector] Init NODE_SOCK_PATH:" << path << " failed. Exiting..."  << std::endl;
		return(EXIT_FAILURE);
	}
	return(EXIT_SUCCESS);
}

int MBDynConnector::negotiate(int timeout, bool verbose, bool dn){
	mbc->SetTimeout(timeout);
	mbc->SetVerbose(verbose);
	mbc->SetDataAndNext(dn);

	if (mbc->Negotiate()) {
		std::cerr << "[MBDyn-connector] MBC Negotiate failed. Exiting...." << std::endl;
		return(EXIT_FAILURE);
	}else{
		std:: cout << "[MBDyn-connector] MBC initialized." << std::endl;
	}
	return(EXIT_SUCCESS);

}

int MBDynConnector::runMBDynProgram(std::string location, std::vector<std::string> strargs){
	pid_t pid;
	int fd;

	if ((pid = fork()) < 0) {
	    std::cerr << "*** ERROR: forking child process failed" << std::endl;
	    return(EXIT_FAILURE);
	}else if (pid == 0) {

		std::string logfile = location+"log.mbdyn";

	    fd = open(logfile.c_str(), O_WRONLY | O_TRUNC | O_CREAT, 0644);
	    if (fd < 0) {
	      perror("[MBDyn-connector] Error opening log file.");
	      exit(EXIT_FAILURE);
	    }

	    if (dup2(fd, 1) != 1) {
	      perror("Error: dup2(fd, 1)");
	      exit(EXIT_FAILURE);
	    }
	    close(fd);

	    if (execlp(strargs[0].c_str(),
	    		   strargs[0].c_str(),	// mbdyn
				   strargs[1].c_str(),	// -f
				   strargs[2].c_str(),	// filename
				   strargs[3].c_str(),	// -o
				   strargs[4].c_str(),	// name
				   strargs[5].c_str(),	// 2>&1
				   (char *)NULL) < 0) {
	        printf("[MBDyn-connector] *** ERROR: exec failed\n");
	        return(EXIT_FAILURE);
	    }
	}
	sleep(2);
	return(EXIT_SUCCESS);
}


void MBDynConnector::computeDisplacements(void){
	for (unsigned int n = 0; n < meshNodes; n++) {
		for(int jj=0;jj<3;jj++){
			connDisplacementDeltas[3*n+jj] = mbc->X(n+1, jj+1) - connCurrentCoords[3*n+jj];
			connDisplacements[3*n+jj] = mbc->X(n+1, jj+1) - connCoords[3*n+jj];
			// not here because we don't know if we converged
			// connCurrentCoords[3*n+jj] = mbc->X(n+1, jj+1);
		}
	}
}


void MBDynConnector::computeForces(double *vec, double coeff){

	if(dim == 3){
		for (unsigned int i = 0; i < 3*meshNodes; i++){
			connForces[i] = vec[i]*coeff;
		}
	}else{
		for (unsigned int ii = 0; ii < meshNodes; ++ii) {
			for (int jj = 0; jj < 2; ++jj) {
				connForces[3*ii+jj] = vec[2*ii+jj]*coeff;
			}
			connForces[3*ii+2] = 0.0;
		}
	}
}


void MBDynConnector::getConnDisplacements(double *vec){

	if(dim == 3){
		if(delta){
			for(unsigned int jj=0; jj<meshNodes*3;jj++){
				vec[jj] = connDisplacementDeltas[jj];
			}
		}else{
			for(unsigned int jj=0; jj<meshNodes*3;jj++){
				vec[jj] = connDisplacements[jj];
			}
		}
	}else{
		if(delta){
			for (unsigned int ii = 0; ii < meshNodes; ++ii) {
				for (int jj = 0; jj < 2; ++jj) {
					vec[dim*ii+jj] = connDisplacementDeltas[3*ii+jj];
				}
			}
			// std::cout << "QW coords : x = " << connCoords[3] << " y = " << connCoords[4] << " z = " << connCoords[5] << std::endl;
			// std::cout << "QW current: x = " << connCurrentCoords[3] << " y = " << connCurrentCoords[4] << " z = " << connCurrentCoords[5] << std::endl;
			// std::cout << "QW displ  : x = " << connDisplacements[3] << " y = " << connDisplacements[4] << " z = " << connDisplacements[5] << std::endl;
			// std::cout << "QW delta  : x = " << connDisplacementDeltas[3] << " y = " << connDisplacementDeltas[4] << " z = " << connDisplacementDeltas[5] << std::endl;

		}else{
			for (unsigned int ii = 0; ii < meshNodes; ++ii) {
				for (int jj = 0; jj < 2; ++jj) {
					vec[dim*ii+jj] = connDisplacements[3*ii+jj];
				}
			}
			// std::cout << "QW coords : x = " << connCoords[3] << " y = " << connCoords[4] << " z = " << connCoords[5] << std::endl;
			// std::cout << "QW current: x = " << connCurrentCoords[3] << " y = " << connCurrentCoords[4] << " z = " << connCurrentCoords[5] << std::endl;
			// std::cout << "QW displ  : x = " << connDisplacements[3] << " y = " << connDisplacements[4] << " z = " << connDisplacements[5] << std::endl;
			// std::cout << "QW delta  : x = " << connDisplacementDeltas[3] << " y = " << connDisplacementDeltas[4] << " z = " << connDisplacementDeltas[5] << std::endl;
		}
	}
}


/*
void MBDynConnector::runSimulation(int iterations){

	for(int i = 0; i < iterations; i++){
		std::cout << "Executing iteration " << i << std::endl;
		setForces(forces);
		//std::cout << "Forces set" << std::endl;
		if(mbc->GetMotion()){
			std::cout << "Break at iteration: " << i << std::endl;
			computeDisplacements();
			writeVTK(i);
			break;
		}
		//std::cout << "get motion" << std::endl;
		mbc->PutForces(true);

		//std::cout << "put forces" << std::endl;
		computeDisplacements();
		if(i%writeinterval == 0){
			writeVTK(i);
		}
		//std::cout << "read displacements" << std::endl;
	}

	// printPositions(displacements);

	mbc->Close();


}
*/

int MBDynConnector::solve(){
	copyForcesToMBDyn();
	int retcode = mbc->GetMotion();
	// computeDisplacements();
	return retcode;
}


void MBDynConnector::copyForcesToMBDyn(){

	for (unsigned int n = 0; n < meshNodes; n++) {
		for(unsigned int jj = 0; jj<3; jj++){
//			if(forces[3*n+jj] != 0.0){
//				std::cout << "#### force at node: " << n+1 << " coordinate: " << jj+1 << " = " << forces[3*n+jj] << std::endl;
//				std::cout << "#### position:  " << mbc->X(n+1,jj+1) << std::endl;
//			}
			mbc->F(n+1,jj+1) = connForces[3*n+jj];
		}
	}
}

void MBDynConnector::putForces(bool converged){
	// std::cout << "####### --- Pre Put Forces. status: " << converged << std::endl;
	mbc->PutForces(converged);

	// if converged we update current coordinates
	if(converged){
		std::cout << "QW converged. we update current coords" << std::endl;
		for (unsigned int n = 0; n < meshNodes; n++) {
			for(int jj=0;jj<3;jj++){
				connCurrentCoords[3*n+jj] = mbc->X(n+1, jj+1);
				connCurrentVelocities[3*n+jj] = mbc->XP(n+1, jj+1);
			}
		}
	}
	// std::cout << "####### --- Post Put Forces. status: " << converged << std::endl;
	return;
}



// reads mesh .dat file (the same file used by mbdyn for structural mapping and fills
// point coordinates data and connectivity (mostly used only for wrtiting vtk file)
// the method also initializes all vectors (coord, current coords, displacements and forces to 0)
int MBDynConnector::readMesh(const char *filename){

	std::ifstream infile(filename);
	std::string line;
	double data[3];
	// unsigned int points[4];

	// try to open file
	if(infile.fail()){
		std::cerr << "[MBDyn-connector] File " << filename << " not opened." << std::endl;
		return(EXIT_FAILURE);
	}

	// read first line which should be "# PROGRAM NUVOLA GRID"
	std::getline(infile, line);
	if(line[0] != '#'){
		std::cerr << "[MBDyn-connector] Initial comment not found." << std::endl;
		return(EXIT_FAILURE);
	}

	// read number of points
	infile >> meshNodes;

	std::cout << "[MBDyn-connector] Number of points in the mesh: " << meshNodes << std::endl;

	// allocate memory for coordinates, displacements, forces and connectivity
	connCoords = (double *)calloc(3*meshNodes, sizeof(double));
	connCurrentCoords = (double *)calloc(3*meshNodes, sizeof(double));
	connDisplacements = (double *)calloc(3*meshNodes, sizeof(double));
	connDisplacementDeltas = (double *)calloc(3*meshNodes, sizeof(double));
	connForces = (double *)calloc(3*meshNodes, sizeof(double));

	connCurrentVelocities = (double *)calloc(3*meshNodes, sizeof(double));

	// mbc->GetMotion();

	for(unsigned int node = 0; node < meshNodes; node++){
		infile >> data[0] >> data[1] >> data[2];
		for(unsigned int jj=0;jj<3;jj++){
			connCoords[3*node+jj] = data[jj];
			connCurrentCoords[3*node+jj] = data[jj];
			// connCoords[3*node+jj] = connCurrentCoords[3*node+jj] = mbc->X(node+1, jj+1);
		}
	}

	infile >> numCells;

	std::cout << "[MBDyn-connector] Number of cells in the mesh: " << numCells << std::endl;

    // allocate memory for cell connectivity
    cells =  new std::vector<int>[numCells];


	// needed twice because otherwise returns empty string, after having read the number of cells
	std::getline(infile, line);

	// std::cout << "empty line? " << line << std::endl;

	for (unsigned int kk = 0; kk < numCells; ++kk) {

		std::getline(infile, line);

		// std::cout << "next line to parse: " << line << std::endl;

		std::stringstream stream(line);

		int current_node;

		while(stream >> current_node){

			// std::cout << "current node: " << current_node << std::endl;

			// remember "NUVOLA GRID is 1 based, here we use 0 based indexing
			cells[kk].push_back(current_node-1);
		}
	}

    // test what we have read
    /*
    for(unsigned int k = 0; k<numCells; k++){
    	std::cout << "cell: " << k << "  ";
    	for(unsigned int j = 0; j<connectivity; j++){
    		std::cout << cells[j+k*connectivity] << " ";
    	}
    	std::cout << std::endl;

    }
    */

	infile.close();

	// initialize the unstructured grid
	initVTK();

	return(EXIT_SUCCESS);
}

// Read coordinates from MBDyn
void MBDynConnector::readCoordinates(){
	mbc->GetMotion();

	for(unsigned int node = 0; node < meshNodes; node++){
		for(unsigned int jj=0;jj<3;jj++){
			connCoords[3*node+jj] = connCurrentCoords[3*node+jj] = mbc->X(node+1, jj+1);
		}
	}
	mbc->PutForces(false);
}

void MBDynConnector::initVTK(){

	points = vtkSmartPointer<vtkPoints>::New();
	unstructuredGrid = vtkSmartPointer<vtkUnstructuredGrid>::New();
	cellsArray = vtkSmartPointer<vtkCellArray>::New();

	int cellsType[numCells];


	for(unsigned int k=0; k<meshNodes; k++){
		points->InsertNextPoint(connCoords[3*k], connCoords[3*k+1], connCoords[3*k+2]);
	}
	unstructuredGrid->SetPoints(points);

	for(unsigned int k=0; k<numCells;k++){

		std::vector <int> :: iterator it;
		int pos = 0;

		switch (cells[k].size()) {
			case 3:
			{
				vtkSmartPointer<vtkTriangle> tri = vtkSmartPointer<vtkTriangle>::New();
				for(it = cells[k].begin(); it != cells[k].end(); ++it){
					tri->GetPointIds()->SetId(pos,*it);
					pos++;
				}
				cellsArray->InsertNextCell(tri);
				cellsType[k] = VTK_TRIANGLE;
				break;
			}

			case 4:
			{
				vtkSmartPointer<vtkQuad> quad = vtkSmartPointer<vtkQuad>::New();
				for(it = cells[k].begin(); it != cells[k].end(); ++it){
					quad->GetPointIds()->SetId(pos,*it);
					pos++;
				}
				cellsArray->InsertNextCell(quad);
				cellsType[k] = VTK_QUAD;
				break;
			}

			default:
			{
				vtkSmartPointer<vtkPolygon> poly = vtkSmartPointer<vtkPolygon>::New();
				for(it = cells[k].begin(); it != cells[k].end(); ++it){
					poly->GetPointIds()->SetId(pos,*it);
					pos++;
				}
				cellsArray->InsertNextCell(poly);
				cellsType[k] = VTK_POLYGON;
				break;
			}
		}
	}

	unstructuredGrid->SetCells(cellsType, cellsArray);

}



void MBDynConnector::writeVTK(int step){


	vtkSmartPointer<vtkDoubleArray> vtkdisplacements = vtkSmartPointer<vtkDoubleArray>::New();
	vtkdisplacements->SetNumberOfComponents(3);
	vtkdisplacements->SetName("Displacement");

	vtkSmartPointer<vtkDoubleArray> vtkdispdeltas = vtkSmartPointer<vtkDoubleArray>::New();
	vtkdispdeltas->SetNumberOfComponents(3);
	vtkdispdeltas->SetName("DisplacementDelta");

	vtkSmartPointer<vtkDoubleArray> vtkforces = vtkSmartPointer<vtkDoubleArray>::New();
	vtkforces->SetNumberOfComponents(3);
	vtkforces->SetName("Force");

	vtkSmartPointer<vtkDoubleArray> vtkvelocities = vtkSmartPointer<vtkDoubleArray>::New();
	vtkvelocities->SetNumberOfComponents(3);
	vtkvelocities->SetName("Velocity");

	vtkIdType idNumPointsInFile = unstructuredGrid->GetNumberOfPoints();

	for (vtkIdType i = 0; i < idNumPointsInFile; i++){
		vtkdisplacements->InsertNextTuple3(connDisplacements[3*i],connDisplacements[3*i+1],connDisplacements[3*i+2]);
		vtkdispdeltas->InsertNextTuple3(connDisplacementDeltas[3*i],connDisplacementDeltas[3*i+1],connDisplacementDeltas[3*i+2]);
		vtkforces->InsertNextTuple3(connForces[3*i],connForces[3*i+1],connForces[3*i+2]);
		vtkvelocities->InsertNextTuple3(connCurrentVelocities[3*i],connCurrentVelocities[3*i+1],connCurrentVelocities[3*i+2]);
	}

	unstructuredGrid->GetPointData()->AddArray(vtkdisplacements);
	unstructuredGrid->GetPointData()->AddArray(vtkdispdeltas);
	unstructuredGrid->GetPointData()->AddArray(vtkforces);
	unstructuredGrid->GetPointData()->AddArray(vtkvelocities);

	/*
	vtkDoubleArray *vec = vtkDoubleArray::New();
	vec->SetComponentName(0,"dx");
	vec->SetComponentName(1,"dy");
	vec->SetComponentName(2,"dz");

	for(unsigned int k=0; k<numCells; k++){
		vec->InsertNextTuple3(displacements[k]-coords[k],displacements[numCells+k]-coords[numCells+k],displacements[2*numCells+k]-coords[2*numCells+k]);
	}

	unstructuredGrid->GetPointData()->SetActiveAttribute("displacements",1);
	unstructuredGrid->GetPointData()->SetVectors(vec);
	*/
	// Write file
	vtkSmartPointer<vtkXMLUnstructuredGridWriter> writer = vtkSmartPointer<vtkXMLUnstructuredGridWriter>::New();
	std::ostringstream ss;

	std::string filename;

	if(step < 0){
		filename = outputname+"init.vtu";
	}else{
		ss << std::setw(5) << std::setfill('0') << step;
		filename = outputname+ss.str()+".vtu";
	}
	writer->SetFileName(filename.c_str());

	if(vtkAscii){
		std::cout << "[MBDyn-Connector] write data in ASCII mode" << std::endl;
		writer->SetDataModeToAscii();
	}

	writer->SetInputData(unstructuredGrid);
	writer->Write();

}

void MBDynConnector::computeResultants(bool print, std::vector<double> root){
	// set to 0
	for(int jj=0;jj<3;jj++){
		Fr[jj] = 0.0;
		Mr[jj] = 0.0;
	}

	for(unsigned int n=0;n<meshNodes;n++){
		for(int jj=0;jj<3;jj++){
			Fr[jj] += connForces[3*n+jj];
		}
		Mr[0] += ((connCurrentCoords[3*n+1]-root[1])*connForces[3*n+2]-(connCurrentCoords[3*n+2]-root[2])*connForces[3*n+1]);
		Mr[1] += ((connCurrentCoords[3*n+2]-root[2])*connForces[3*n]-(connCurrentCoords[3*n]-root[0])*connForces[3*n+2]);
		Mr[2] += ((connCurrentCoords[3*n]-root[0])*connForces[3*n+1]-(connCurrentCoords[3*n+1]-root[1])*connForces[3*n]);
	}
	if(print){
		fprintf(stdout,"F Resultant:");
		fprintf(stdout, "%+16.4e %+16.4e %+16.4e\n", Fr[0], Fr[1], Fr[2]);
		fprintf(stdout,"M Resultant:");
		fprintf(stdout, "%+16.4e %+16.4e %+16.4e\n", Mr[0], Mr[1], Mr[2]);
	}
}


void MBDynConnector::writeResultants(std::ofstream& file, double c){
	file << Fr[0] << " " << Fr[1] << " " << Fr[2] << " " << Mr[0] << " " << Mr[1] << " " << Mr[2] << " " << c << std::endl;
}


void MBDynConnector::printVector(int type){

	std::string name;
	double *vec = nullptr;

	switch (type) {
		case 0:{		// coordinates
			vec = connCoords;
			name = "Coordinates";
			break;
		}
		case 1:{
			vec = connCurrentCoords;
			name = "Current Coordinates";
			break;
		}
		case 2:{
			vec = connDisplacements;
			name = "Displacements";
			break;
		}
		case 3:{
			vec = connForces;
			name = "Forces";
			break;
		}
		default:{
			std::cout << "Unrecognized vector type" << std::endl;
			break;
			return;
		}
	}
	std::cout << name << std::endl;
	std::cout << "x\t" << "y\t" << "z" << std::endl;
	//for (unsigned int n = 0; n < meshNodes; n++) {
	for (unsigned int n = 0; n < 10; n++) {
		fprintf(stdout, "%+16.4e %+16.4e %+16.4e\n", vec[3*n], vec[3*n+1], vec[3*n+2]);
	}
}

void MBDynConnector::setDimensions(int problemDim){
	std::cout << "[MBDyn-connector] Setting problem dimension to " << problemDim << std::endl;
	dim = problemDim;
}

MBDynConnector::~MBDynConnector(void){
	// std::cout << "[MBDyn-connector] closing tip displacement file." << std::endl;
	// tipDisplacement.close();
	// TODO: clean
}
