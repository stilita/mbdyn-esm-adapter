/*
 * MBDynAdapter.cpp
 *
 *  Created on: Feb 19, 2020
 *      Author: claudio
 */



#include <unistd.h>
#include <cstdlib>
#include <cstdio>
#include <iostream>
#include <fstream>
#include <algorithm> 
#include <cmath>
#include <rapidjson/document.h>
#include <rapidjson/filereadstream.h>

#include "MBDynAdapter.h"



MBDynAdapter::MBDynAdapter(const char* filename){
	conn = new MBDynConnector();

	// open cofiguration file
	FILE* fp = fopen(filename, "r");

	char readBuffer[65536];
	rapidjson::FileReadStream is(fp, readBuffer, sizeof(readBuffer));

	document.ParseStream(is);

	fclose(fp);

	assert(document.IsObject());

	dummyCFD = new InterfaceForce(&document);

}

int MBDynAdapter::initializeConnector(){


	// set vtk output name
	if(document.HasMember("vtk-output")){
		std::cout << "[MBDyn-adapter] setup file name for VTU output" << std::endl;
		conn->setOutputName(document["vtk-output"].GetString());
	}else{
		std::cout << "[MBDyn-adapter] WARNING: using default file name for VTU output" << std::endl;
		conn->setOutputName("./output/test01_");
	}



	assert(document.HasMember("mesh"));
	if(conn->readMesh(document["mesh"].GetString())){
		std::cerr << "[MBDyn-adapter] Mesh file not read. Exiting..." << std::endl;
		return(EXIT_FAILURE);
	}

	// MBDyn log file location
	std::string location;

	if(document.HasMember("mbdyn-log-location")){
		std::cout << "[MBDyn-adapter] set directory for MBDyn log file..." << std::endl;
		location = document["mbdyn-log-location"].GetString();
		// check if last character is slash
		if(location.back() != '/'){
			location.append("/");
		}

	}else{
		location = "./";
		std::cout << "[MBDyn-adapter] Using current directory as location for MBDyn log file..." << std::endl;
	}



	// vector of strings containing command line arguments to be used to launch mbdyn
	std::vector<std::string> strargs;

	// command
	strargs.push_back("mbdyn");
	// -f option
	strargs.push_back("-f");
	// input filename
	assert(document.HasMember("mbdyn-input"));
	strargs.push_back(document["mbdyn-input"].GetString());
	// -o option
	strargs.push_back("-o");
	// output files root
	assert(document.HasMember("mbdyn-output"));
	strargs.push_back(document["mbdyn-output"].GetString());
	// redirect stderr to stdout
	strargs.push_back("2>&1");

	if(conn->runMBDynProgram(location, strargs)){
		std::cerr << "[MBDyn-adapter] MBDyn run failed. Exiting..." << std::endl;
		return(EXIT_FAILURE);
	}else{
		std::cout << "[MBDyn-adapter] MBDyn started with input file: " << strargs[2] << std::endl;
	}

	// Ext Node parameters (as seen in the example)
	MBCBase::Rot refnoderot = MBCBase::NONE;
	unsigned int nodes = conn->getMeshNodes();
	bool labels = false;
	MBCBase::Rot rot = MBCBase::NONE;
	bool accelerations = false;
	int timeout = -1;
	bool verbose = true;
	bool data_and_next = false;

	if(conn->initializeMBCNodal(refnoderot, nodes, labels, rot, accelerations)){
		std::cerr << "[MBDyn-adapter] Connector initialization failed, Exiting..." << std::endl;
		return(EXIT_FAILURE);
	}

	if(document.HasMember("node-socket")){
		std::cout << "[MBDyn-adapter] Socket defined in config file." << std::endl;
		if(conn->initializeSocket(document["node-socket"].GetString())){
			return(EXIT_FAILURE);
		}
	}else{
		std::cout << "[MDByn-adapter] WARNING: Using default socket" << std::endl;
		if(conn->initializeSocket("/tmp/mbdyn.node.sock")){
			return(EXIT_FAILURE);
		}
	}


	if(conn->negotiate(timeout, verbose, data_and_next)){
		return(EXIT_FAILURE);
	}

	// set output file format
	if(document.HasMember("vtk-ascii")){
		std::cout << "[MBDyn-adapter] setting VTU output format to ";
		conn->vtkAscii = document["vtk-ascii"].GetBool();
		if(conn->vtkAscii){
			std::cout << "ASCII" << std::endl;
		}else{
			std::cout << "binary" << std::endl;
		}
	}else{
		std::cout << "[MBDyn-adapter] defaulting VTU output format to binary" << std::endl;
	}


	if(document["read-coords"].GetBool()){
		// Note: -1 produces a "_init.vtu" file
		conn->writeVTK(-1);
		std::cout << "[MBDyn-adapter] read initial coordinates from MBDyn" << std::endl;
		conn->readCoordinates();
	}


	return(EXIT_SUCCESS);

}


int MBDynAdapter::initializeAdapter(){

	if(document.HasMember("displacement-delta")){
		std::cout << "[MBDyn-adapter] setup displacement deltas from config file" << std::endl;
		conn->delta = document["displacement-delta"].GetBool();
	}

	// get problem dimension:
	// all mbdyn problems are 3D
	dim = 3;

	std::cout << "[MBDyn-adapter] number of dimensions: " << dim << std::endl;

	// send size to connector
	conn->setDimensions(dim);

	// get number of vertices from connector who has read the mesh file
	vertexSize = conn->getMeshNodes(); // number of vertices at wet surface

	adapterCoords = (double *)calloc(dim*vertexSize, sizeof(double));
	adapterDisplacements = (double *)calloc(dim*vertexSize, sizeof(double));
	adapterForces = (double *)calloc(dim*vertexSize, sizeof(double));

	// std::cout << "Memory allocation" << std::endl;

	// copy coordinates from connector, discarding z if problem dimension is 2
	if(dim == 3){
		for (int ii = 0; ii < dim*vertexSize; ++ii) {
			adapterCoords[ii] = conn->connCoords[ii];
		}
	}else{
		for (int ii = 0; ii < vertexSize; ++ii) {
			for (int jj = 0; jj < 2; ++jj) {
				adapterCoords[dim*ii+jj] = conn->connCoords[3*ii+jj];
			}
		}
	}

	// solver timestep size
	// maximum PreCICE timestep size
	// TODO: how does it relate to all the timesteps?
	if(document.HasMember("dt")){
		dt = document["dt"].GetFloat();
	}else{
		dt = 0.001;
		std::cout << "[MBDyn-adapter] WARNING: using default timestep" << std::endl;
	}
	std::cout << "[MBDyn-adapter] simulation dt: " << dt << std::endl;

	// should not be necessary:
	conn->computeForces(adapterForces,1.0);
	//std::cout << "Compute forces" << std::endl;


	conn->writeVTK(0);
	std::cout << "[MBDyn-adapter] Write iteration 0 vtu" << std::endl;

	return(EXIT_SUCCESS);
}

void MBDynAdapter::runSimulation(){
	// define strings used for write and read checkpoints

	int iteration = 0;
	// int tmp_it = 0;
	double coeff = 0.0;

	// for step ramp
	double stepsize = 0.0;
	int timesteps = 0;
	double steptime = 0.0;


	std::vector<unsigned int> tipNodes{ 0 };


	// retrieve simulation info from json
	assert(document.HasMember("iterstart"));
	int iterstart = document["iterstart"].GetInt();
	assert(document.HasMember("period"));
	int period = document["period"].GetInt();
	assert(document.HasMember("write-interval"));
	int write_interval = document["write-interval"].GetInt();

	std::vector<double> root;

	assert(document["root-coords"].IsArray());
	for (rapidjson::SizeType i = 0; i < document["root-coords"].Size(); i++){ // Uses SizeType instead of size_t
		root.push_back(document["root-coords"][i].GetDouble());
	}

	std::string ramp_string(document["ramp-type"].GetString(), document["ramp-type"].GetStringLength());

	int ramp_type = 0;

	if(ramp_string == "linear"){
		ramp_type = 1;
		std::cout << "[MBDyn-adapter] Using linear progression for fluid forces." << std::endl;
	}else if(ramp_string == "cos"){
		ramp_type = 2;
		std::cout << "[MBDyn-adapter] Using (1-cos) progression for fluid forces." << std::endl;
	}else if(ramp_string == "step"){
		ramp_type = 3;
		std::cout << "[MBDyn-adapter] Using STEP progression for fluid forces." << std::endl;
	}else if(ramp_string == "none"){
		std::cout << "[MBDyn-adapter] NO progression for fluid forces." << std::endl;
	}else{
		std::cout << "[MBDyn-adapter] Progression for fluid forces not parsed. Using none" << std::endl;
	}

	// initial coefficient for forces.
	assert(document.HasMember("coeff0"));
	double coeff0 = document["coeff0"].GetDouble();

	if(ramp_type == 3){
		assert(document.HasMember("step-size"));
		stepsize = document["step-size"].GetDouble();
		timesteps = (int)ceil((1-coeff0)/stepsize);
		steptime = period/(timesteps-1);
	}

	// Create a file to write force at each timestep (used for debug/analysis purposes)
	// open file to write forces
	assert(document.HasMember("resultant-file"));
	std::ofstream resultantFile(document["resultant-file"].GetString());
	resultantFile << "#t Rx Ry Rz Mx My Mz coeff" << std::endl;


	bool write_adapter_disp = false;
	std::ofstream tipDisplacement("tip.txt");



	if(document.HasMember("num-repeat")){
		maxRepeat = document["num-repeat"].GetUint();
	}else{
		std::cout << "[MBDyn-adapter] WARNING using default number of 1 max repetition of timestep" << std::endl;
	}


	int currentRepeat = 0;
	bool running = true;
	bool final_step = false;



	// Start coupling:
	while (running){

		// copy forces to connector with coefficient
		if(iteration<iterstart){
			coeff = coeff0;
		}else if(iteration <= iterstart + period){
			switch (ramp_type) {
				case 1:{
					coeff = coeff0 + (1.0-coeff0)*(std::min((iteration-iterstart),period)/((double)period));
					break;
				}
				case 2:{
					//std::cout << "----------COS------------" << std::endl;
					coeff = coeff0 + (1.0-coeff0)*(0.5*(1-cos(M_PI*std::min((iteration-iterstart),period)/period)));
					break;
				}
				case 3:{
					int k = (int)ceil((iteration - iterstart)/steptime);
					coeff = std::min(coeff0 + k*stepsize, 1.0);
					break;
				}
				default:{
					coeff = 1.0;
					break;
				}
			}
		}else{
			coeff = 1.0;
		}

		// std::cout << "iteration: " << iteration << " iterstart: " << iterstart << " period: " << period << std::endl;

		if(ramp_type == 3){
			std::cout << "[MBDyn-adapter] forces coefficient: " << coeff << " step size: " << stepsize << " timesteps: " << timesteps << " step time: " << steptime << std::endl;
		}else{
			std::cout << "[MBDyn-adapter] forces coefficient: " << coeff << std::endl;
		}

		// read forces from interface
		dummyCFD->getInterfaceForces(adapterForces, vertexSize, t);


		conn->computeForces(adapterForces,coeff);

		conn->writeTip(tipDisplacement, tipNodes, t, "begin");

		if(write_adapter_disp){
			writeTip(tipDisplacement, tipNodes, t, "ADAPTER begin");
		}


		conn->setOutputName("./output/preSolve_iter_");
		conn->writeVTK(iteration);

		// MBDyn compute motion step
		std::cout << "compute motion step" << std::endl;

		final_step = conn->solve();
		conn->putForces(false);
		final_step = conn->solve();


		conn->writeTip(tipDisplacement, tipNodes, t, "after solve");

		conn->computeDisplacements();

		// retrieve displacements from connector, which depend on problem dimension
		conn->getConnDisplacements(adapterDisplacements);

		if(write_adapter_disp){
			writeTip(tipDisplacement, tipNodes, t, "ADAPTER after copying");
		}

		conn->setOutputName("./output/postSolve_iter_");
		conn->writeVTK(iteration);


		conn->computeResultants(true, root);

		std::cout << "STEP: " << iteration << " time: " << t << std::endl;

		// Checkpoint
		/*
		if(interface.isActionRequired(cowic)){
			// When an implicit coupling scheme is used, checkpointing is required
			// stub of function as described in the adapter example
			// saveOldState(); // save checkpoint
			std::cout << "--- cowic 2 ---" << std::endl;
			std::cout << cowic << std::endl;
			interface.fulfilledAction(cowic);
		}
		*/

		// if(interface->isActionRequired(coric)){
		if(currentRepeat < maxRepeat){
			// timestep not converged
			// reloadOldState(); // set variables back to checkpoint
			// interface->markActionFulfilled(coric);
			std::cout << "---- NOT Converged ----" << std::endl;
			conn->putForces(false);

			conn->writeTip(tipDisplacement, tipNodes, t, "after putForces false");

			conn->computeDisplacements();

			// retrieve displacements from connector, which depend on problem dimension
			conn->getConnDisplacements(adapterDisplacements);

			if(write_adapter_disp){
				writeTip(tipDisplacement, tipNodes, t, "after copying after putForces false");
			}

			conn->setOutputName("./output/nc_iter_"+std::to_string(currentRepeat)+"_");
			conn->writeVTK(iteration);
			if(document.HasMember("vtk-output")){
				conn->setOutputName(document["vtk-output"].GetString());
			}else{
				conn->setOutputName("./output/test01_");
			}
			currentRepeat++;

		}else{
			// timestep converged
			conn->putForces(true);

			conn->writeTip(tipDisplacement, tipNodes, t, "after putForces true");

			std::cout << "**** Converged ****" << std::endl;

			conn->computeDisplacements();

			// retrieve displacements from connector, which depend on problem dimension
			conn->getConnDisplacements(adapterDisplacements);

			if(write_adapter_disp){
				writeTip(tipDisplacement, tipNodes, t, "ADAPTER after copying after putForces true");
			}

			if(document.HasMember("vtk-output")){
				conn->setOutputName(document["vtk-output"].GetString());
			}else{
				conn->setOutputName("./output/test01_");
			}

			conn->writeVTK(iteration);



			// conn->writeVTK(iteration);
			if( ( iteration % write_interval ) == 0 ){
				conn->writeVTK(iteration);
			}

			resultantFile << t << " ";
			conn->writeResultants(resultantFile, coeff);
			iteration++;
			t += dt;

			if(final_step){
				conn->writeVTK(iteration);
				running = false;
			}

			currentRepeat = 0;
		}
	}

	std::cout << "[MBDyn-adapter] Simulation finished" << std::endl;

}


void MBDynAdapter::finalize(){
	// interface->finalize();
	delete conn;
	std::cout << "[MBDyn-adapter] Finalized" << std::endl;
}


void MBDynAdapter::writeTip(std::ofstream& fileName, std::vector<unsigned int> nodes, float time, std::string comment){
	for(std::vector<unsigned int>::iterator node = nodes.begin() ; node != nodes.end(); ++node){
		fileName << std::setw(2) << *node << "\t" << std::setw(6) << time;
		for (int coord = 0; coord < dim; ++coord) {
			fileName << "\t" << std::setw(12) << adapterDisplacements[(*node)*dim + coord];
		}
		fileName << "\t" << comment << "\n";
	}
}



void MBDynAdapter::printVector(int type){

	std::string name;
	double *vec = nullptr;

	switch (type) {
		case 0:{		// coordinates
			vec = adapterCoords;
			name = "ADAPTER Coordinates";
			break;
		}
		case 1:{
			//vec = currentCoords;
			//name = "Current Coordinates";
			std::cout << "ADAPTER current coords not available..." << std::endl;
			return;
			break;
		}
		case 2:{
			vec = adapterDisplacements;
			name = "ADAPTER Displacements";
			break;
		}
		case 3:{
			vec = adapterForces;
			name = "ADAPTER Forces";
			break;
		}
		default:{
			std::cout << "Unrecognized vector type" << std::endl;
			return;
		}
	}
	std::cout << name << std::endl;
	std::cout << "x\t" << "y\t" << "z" << std::endl;
	// for (int n = 0; n < vertexSize; n++) {
	for (int n = 0; n < 10; n++) {
		fprintf(stdout, "%+16.4e %+16.4e %+16.4e\n", vec[3*n], vec[3*n+1], vec[3*n+2]);
	}
}

MBDynAdapter::~MBDynAdapter(void){
	//std::cout << "[MBDyn-adapter] closing tip displacement file." << std::endl;
	//tipDisplacement.close();
	// TODO clean
}


