#! /bin/bash
gnuplot -p << EOF
set grid
set title 'Displacement of the Flap Tip'
set xlabel 'Time [s]'
set ylabel 'Y-Displacement [m]'
plot "./precice-Solid-watchpoint-tip.log" using 1:8 with lines title ""
EOF

