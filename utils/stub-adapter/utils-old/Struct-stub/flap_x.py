import getopt
import json
import sys

import numpy as np
# from mpi4py import MPI
import precice
import vtk


def main(argv):
    try:
        opts, args = getopt.getopt(argv,"hf:",["file="])
    except getopt.GetoptError:
        print('struct-stub.py -f <inputfile.json>')
        sys.exit(2)
    for opt, arg in opts:
        if opt == '-h':
            print('struct-stub.py -f <inputfile.json>')
            sys.exit()
        elif opt in ("-f", "--file"):
            inputfile = arg

    np.set_printoptions(precision=7)

    with open(inputfile, 'r') as f:
        config = json.load(f)

    struct = StructStub(config)
    struct.run()


class StructStub:
    def __init__(self, config):

        self.config = config

        #interface = precice.Interface(solverName, configFileName, processRank, processSize)
        self.interface = precice.Interface(config["participantName"], config["precice-config"], 0, 1) # proc no, nprocs
        self.dim = self.interface.get_dimensions()
        self.num_nodes = 0

        self.read_mesh(config["mesh"])

        # self.compute_areas()
        self.delta = config["displacement-is_delta"]
        self.wrt_interval = config["write-interval"]

        nmeshID = self.interface.get_mesh_id(config["meshName"])

        self.nodeVertexIDs = self.interface.set_mesh_vertices(nmeshID, self.nodes)

        # data from interface
        self.force = np.zeros((self.num_nodes,self.dim))
        # total displacement
        self.displacement = np.zeros_like(self.force)
        # is_delta displacement from 2 iterations
        self.displacement_delta = np.zeros_like(self.force)

        self.forceID = self.interface.get_data_id(config["writeDataName"], nmeshID)
        self.displacementID = self.interface.get_data_id(config["readDataName"], nmeshID)

        self.A0 = 0.075
        self.period = 2.0
        self.t = 0.0
        self.L = 0.35
        
        self.dt = self.interface.initialize()
        
        if self.interface.is_action_required(precice.action_write_initial_data()):
            if self.delta:
                self.interface.write_block_vector_data(self.displacementID, self.nodeVertexIDs, self.displacement_delta)
            else:
                self.interface.write_block_vector_data(self.displacementID, self.nodeVertexIDs, self.displacement)
            self.interface.mark_action_fulfilled(precice.action_write_initial_data())
        
        self.interface.initialize_data()
        
        if self.interface.is_read_data_available():
            self.force = self.interface.read_block_vector_data(self.forceID, self.nodeVertexIDs)
            print("data available")
        else:
            print("NO data available")

    def read_mesh(self, filename):
        with open(filename, 'r') as mf:

            print("Read mesh")
            r = mf.readline()

            while r[0] == '#':
                r = mf.readline()

            try:
                self.num_nodes = int(r)
            except ValueError as ve:
                print(ve)
                print("Number of nodes not found!")
                print("Extiting...")
                return

            self.nodes = np.zeros((self.num_nodes, self.dim))

            for i in range(self.num_nodes):
                point = mf.readline().split()
                if self.dim < len(point):
                    self.nodes[i, :] = [float(coord) for coord in point[0:self.dim]]
                else:
                    self.nodes[i, :] = [float(coord) for coord in point]

            # read number of cells
            r = mf.readline()
            try:
                self.num_cells = int(r)
            except ValueError as ve:
                print(ve)
                print("Number of cells not found!")
                print("Extiting...")
                return

            self.cells = np.zeros((self.num_cells, 4),dtype=np.int16)

            if self.dim == 3:
                self.cell_areas = np.zeros(self.num_cells)
                self.normals = np.zeros((self.num_cells, 3))

            for i in range(self.num_cells):
                connectivity = mf.readline().split()
                self.cells[i, :] = [int(node)-1 for node in connectivity]

        print("Mesh read")
        self.coords = self.nodes.copy()
        # print(self.nodes[0,:])
        # print(self.nodes[1, :])
        # print(self.nodes[14, :])
        # print(self.nodes[23, :])
        # print(self.nodes[12,:])
        # print(self.nodes[4, :])
        # print(self.nodes[5, :])
        # print(self.nodes[13, :])

    def compute_areas_and_normals(self):

        for i in range(self.num_cells):
            point_a = self.coords[self.cells[i,0],:]
            point_b = self.coords[self.cells[i,1],:]
            point_c = self.coords[self.cells[i,2],:]

            segm_ab = point_b - point_a
            segm_bc = point_c - point_b

            self.cell_areas[i] = np.linalg.norm(segm_ab)*np.linalg.norm(segm_bc)
            tempv = np.cross(segm_ab,segm_bc)
            self.normals[i,:] = tempv/np.linalg.norm(tempv)

            # if i==44 or i==45:
            #     print("----AREA----")
            #     print("Point A: x:{0} y:{1} z{2}".format(point_a[0],point_a[1],point_a[2]))
            #     print("Point B: x:{0} y:{1} z{2}".format(point_b[0],point_b[1],point_b[2]))
            #     print("Point C: x:{0} y:{1} z{2}".format(point_c[0],point_c[1],point_c[2]))
            #     print("Segmento AB: {0} - lunghezza: {1}".format(segm_ab,np.linalg.norm(segm_ab)))
            #     print("Segmento BC: {0} - lunghezza: {1}".format(segm_bc,np.linalg.norm(segm_bc)))
            #     print("Normale: x:{0} y:{1} z:{2}".format(self.normals[i,0],self.normals[i,1],self.normals[i,2]))
            #     print("AREA: {0}".format(self.cell_areas[i]))


        # print("normals computed. NB into the solid")
        # print(self.cell_areas[44:46])
        # print(self.normals)
        # print(self.nodes)
        # print(self.cells)
        #print(self.coords)
        #print(pippo)

    def compute_resultants(self):
        root = np.zeros((3,1))
        self.F = np.sum(self.force,axis=0)
        self.M = np.zeros_like(self.F)

        self.M[0] = np.dot(self.coords[:, 1], self.force[:, 2]) - np.dot(self.coords[:, 2], self.force[:, 1])
        self.M[1] = np.dot(self.coords[:, 2], self.force[:, 0]) - np.dot(self.coords[:, 0], self.force[:, 2])
        self.M[2] = np.dot(self.coords[:, 0], self.force[:, 1]) - np.dot(self.coords[:, 1], self.force[:, 0])

    def set_displacement(self):
        
        self.displacement = np.zeros_like(self.nodes)
        
        At = 0.5*self.A0 * (1.0 - np.cos(2 * np.pi / self.period * self.t))
        print("t = {0} - At = {1}".format(self.t,At))
        angle = np.arctan(2*At/(self.L**2))*self.nodes[:, 0]
        self.displacement[:, 0] = -self.nodes[:, 1]*np.sin(angle)
        self.displacement[:, 1] = At * ( self.nodes[:, 0] / self.L) ** 2 + self.nodes[:, 1]*(1-np.cos(angle))

        # print(self.displacement[:,0])

        self.displacement_delta = self.displacement - self.coords

    def run(self):
        self.iteration = 0

        tmp_it = 0

        self.write_vtk('./output/struct')

        f_file = open('struct_forces.dat','w')
        m_file = open('struct_moments.dat', 'w')

        while self.interface.is_coupling_ongoing():
            if self.interface.is_action_required(precice.action_write_iteration_checkpoint()):
                self.interface.mark_action_fulfilled(precice.action_write_iteration_checkpoint())

            self.force = self.interface.read_block_vector_data(self.forceID, self.nodeVertexIDs)

            if self.dim == 3:
                self.compute_areas_and_normals()
            
            self.set_displacement()

            if self.delta:
                self.interface.write_block_vector_data(self.displacementID, self.nodeVertexIDs, self.displacement_delta)
            else:
                self.interface.write_block_vector_data(self.displacementID, self.nodeVertexIDs, self.displacement)
            
            
            self.interface.advance(self.dt)

            # if self.iteration > 11:
            #     print("FORCING STOP...")
            #     break

            if self.interface.is_action_required(precice.action_read_iteration_checkpoint()): # i.e. not yet converged
                self.interface.mark_action_fulfilled(precice.action_read_iteration_checkpoint())

                if(self.config["every-iteration"]):
                    self.write_vtk('./output/subit_{0:02d}'.format(tmp_it))
                    tmp_it += 1

            else:
                tmp_it = 0

                self.coords = self.nodes + self.displacement

                #if self.iteration % 10 == 0:
                self.iteration += 1
                if self.iteration % self.wrt_interval == 0:
                    self.write_vtk('./output/struct')
                self.t += self.dt

                #print(np.reshape(self.displacement,(-1,3))[:20,:])
                #print(self.coords[:20,:])

                print("iteration {}".format(self.iteration))

                self.compute_resultants()

                f_file.write("{0:.4f}   {1:.6f}   {2:.6f}   {3:.6f}\n".format(self.t, *self.F))
                m_file.write("{0:.4f}   {1:.6f}   {2:.6f}   {3:.6f}\n".format(self.t, *self.M))

    def write_vtk(self, name):

        grid = vtk.vtkUnstructuredGrid()
        grid.Allocate(1, 1)

        points = vtk.vtkPoints()
        points.SetNumberOfPoints(self.num_nodes)

        for i in range(self.num_nodes):
            if self.dim == 3:
                points.InsertPoint(i,self.nodes[i,:])
            else:
                points.InsertPoint(i, np.append(self.nodes[i, :],0.0))

        grid.SetPoints(points)

        for i in range(self.num_cells):
            quad = vtk.vtkQuad()
            for k in range(4):
                quad.GetPointIds().SetId(k, self.cells[i,k])

            grid.InsertNextCell(quad.GetCellType(), quad.GetPointIds())

        vtk_force = vtk.vtkDoubleArray()
        vtk_force.SetNumberOfComponents(3)
        # vtk_force.SetNumberOfValues(self.num_nodes)
        vtk_force.SetName("force")

        vtk_displacement = vtk.vtkDoubleArray()
        vtk_displacement.SetNumberOfComponents(3)
        # vtk_displacement.SetNumberOfValues(self.num_nodes)
        vtk_displacement.SetName("displacement")

        vtk_disp_delta = vtk.vtkDoubleArray()
        vtk_disp_delta.SetNumberOfComponents(3)
        # vtk_displacement.SetNumberOfValues(self.num_nodes)
        vtk_disp_delta.SetName("displacement-is_delta")

        if self.dim == 3:
            for i in range(self.num_nodes):
                vtk_force.InsertNextTuple(self.force[i,:])
                vtk_displacement.InsertNextTuple(self.displacement[i,:])
                vtk_disp_delta.InsertNextTuple(self.displacement_delta[i,:])

            vtk_normal = vtk.vtkFloatArray()
            vtk_normal.SetNumberOfComponents(3)
            # vtk_normal.SetNumberOfValues(self.num_cells)
            vtk_normal.SetName("normal")

            vtk_area = vtk.vtkFloatArray()
            #vtk_area.SetNumberOfComponents(1)
            # vtk_area.SetNumberOfValues(self.num_cells)
            vtk_area.SetName("area")

            for i in range(self.num_cells):
                vtk_normal.InsertNextTuple3(self.normals[i,0],self.normals[i,1],self.normals[i,2])
                vtk_area.InsertNextValue(self.cell_areas[i])

            grid.GetCellData().AddArray(vtk_normal)
            grid.GetCellData().AddArray(vtk_area)

        else:
            for i in range(self.num_nodes):
                vtk_force.InsertNextTuple(np.append(self.force[i,:],0.0))
                vtk_displacement.InsertNextTuple(np.append(self.displacement[i,:],0.0))
                vtk_disp_delta.InsertNextTuple(np.append(self.displacement_delta[i,:],0.0))


        grid.GetPointData().AddArray(vtk_force)
        grid.GetPointData().AddArray(vtk_displacement)
        grid.GetPointData().AddArray(vtk_disp_delta)

        writer = vtk.vtkXMLUnstructuredGridWriter()
        #writer.SetDataModeToBinary()
        writer.SetFileName(name+'_{0:05d}.vtu'.format(self.iteration))
        writer.SetInputData(grid)
        # writer.SetDataModeToAscii()
        writer.Write()

        # writer = vtk.vtkUnstructuredGridWriter()
        # writer.SetInputData(grid)
        # writer.SetFileName("./output/Output.vtu")
        # writer.Write()


if __name__ == "__main__":
    main(sys.argv[1:])
