import numpy as np

def mesh_reorder():

    points = np.genfromtxt("./mesh/points_order.txt")

    orig_points = np.genfromtxt("./mesh/root0_points.dat")

    orig_conn = np.genfromtxt("./mesh/root0_conn.dat",dtype=np.uint16)

    num_points = np.shape(points)[0]
    num_cells = np.shape(orig_conn)[0]

    reorder = np.zeros((num_points), dtype=np.int16)
    distances = np.zeros((num_points))

    # print(orig_conn)

    for l in range(num_points):
        distance = 2.0
        for k in range(num_points):
            dist = (np.sqrt(np.sum((orig_points[l,:]-points[k,:])**2)))
            if dist < distance:
                distance = dist
                reorder[l] = k
                distances[l] = distance

    # for l in range(num_points):
    #     print("{0:d} {1:f}\n".format(reorder[l],distances[l]))

    #print(reorder)

    with open("./mesh/reordered_root0.dat", 'w') as mf:

        mf.write("# Program NUVOLA GRID\n")
        mf.write(" {0:d}\n".format(num_points))

        for line in range(num_points):
            mf.write(" {0}    {1}    {2}\n".format(*points[line,:]))

        mf.write(" {0:d}\n".format(num_cells))

        for line in range(num_cells):
            for k in range(4):
                mf.write("{0:d}    ".format(reorder[orig_conn[line,k]-1]+1))
            mf.write("\n")


if __name__ == "__main__":
    mesh_reorder()