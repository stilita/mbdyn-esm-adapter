#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun May 10 10:42:34 2020

@author: claudio
"""


import numpy as np

def read_disp(filename):
    with open(filename) as r:
        title = r.readline()
    
    delta = 'Delta' in title
    
    tip_data = np.genfromtxt(filename, skip_header = 1)
    
    time = tip_data[:,0]
    
    if tip_data.shape[1] == 10:
        # 3D data
        dim = 3
    else:
        dim = 2
        
    coords = tip_data[0,1:dim+1]
    force = tip_data[:,1+dim:2*dim+1]
        
    if delta:
        displacement = np.cumsum(tip_data[:,2*dim+1:],axis=0)
        disp_delta = tip_data[:,2*dim+1:]
    else:
        displacement = tip_data[:,2*dim+1:]
        disp_delta = np.zeros_like(displacement)
        disp_delta[1:,:] = displacement[1:,:] - displacement[0:-1,:]
    
    return time, coords, force, displacement, disp_delta
        